from openjdk:8-jdk-slim-bullseye
ENV LANG C.UTF-8
ENV TZ Asia/Shanghai
COPY target/cn-cjd-springboot-1.0.0.0-SNAPSHOT.jar /
COPY independent.yml /
#ENTRYPOINT java -Xms128M -Xmx128M -XX:NewRatio=2 -Xss256K -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:/data/jvm/gc.log -jar /cn-cjd-springboot-1.0.0.0-SNAPSHOT.jar
ENTRYPOINT java -jar /cn-cjd-springboot-1.0.0.0-SNAPSHOT.jar --Dspring.config.location=/
