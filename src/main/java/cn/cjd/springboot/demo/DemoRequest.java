package cn.cjd.springboot.demo;

import com.alibaba.fastjson.JSON;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * project  DSMP
 * description: module
 *
 * @author chengjiade
 * date:    2022/2/9 11:32 上午
 * @version v1.0
 */
public class DemoRequest {

    public static void demo() throws Exception {
        HttpUtil httpUtil = new HttpUtil("http://192.168.180.127",
                "642867bf59e14afea91257766bea58ce",
                "243778b773574f058bc8a2676199e61a");
        List<NameValuePair> params = new ArrayList<NameValuePair>() {{
            add(new BasicNameValuePair("projectNo", "3"));
            add(new BasicNameValuePair("level", "4"));
        }};
        String result = httpUtil.get("/openapi/project/v1/getSecurityInfo", params);
        System.out.println(JSON.toJSONString(result));
    }

    public static void main(String[] args) throws Exception {
        demo();
    }
}
