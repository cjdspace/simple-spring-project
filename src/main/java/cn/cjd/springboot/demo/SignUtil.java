package cn.cjd.springboot.demo;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * project  dsmp_jenkins_plugin
 * description: module
 * openApi 校验码生成
 *
 * @author chengjiade
 * date:    2021/10/28 3:50 下午
 * @version v1.0
 */
public class SignUtil {

    /**
     * 生成签名，可生成签名用于OpenApi接口
     *
     * @param timestamp 校验的时间戳，单位秒， 注意时区，否则容易出错
     * @param method    接口请求方式， GET、POST
     * @param params    请求的非body参数，接口url中的参数， 例如：?projectName=1
     * @param body      body参数的toString， 没有body传""
     */
    public static String createSign(Long timestamp, String method, Map<String, String> params, String body, String sk) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(">>>> ts: " + timestamp);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = sdf.format(new Date(Long.parseLong(timestamp + "000")));
        System.out.println("+++> date: " + date);

        // 参数按Key排序
        System.out.println(params);
        Map<String, String> sortedParams = new LinkedHashMap<>(params.size());
        params.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(e -> sortedParams.put(e.getKey(), e.getValue()));

        System.out.println(sortedParams);

        // 参数key和value进行编码后连接成字符串
        StringBuilder sb = new StringBuilder();
        for (String key : sortedParams.keySet()) {
            System.out.println(key + ":" + sortedParams.get(key));
            String k = URLEncoder.encode(key, "utf-8").replaceAll("\\+", "%20");
            String v = URLEncoder.encode(sortedParams.get(key), "utf-8").replaceAll("\\+", "%20");
            sb.append(k).append("=").append(v).append("&");
        }
        if (sb.length() > 0) {
            sb.delete(sb.length() - 1, sb.length());
        }
        System.out.println("--> CanonicalizedQueryString: " + sb);

        String hashedRequestPayload = sha256Hex(body);
        System.out.println("--> body: " + body);
        System.out.println("---> HashedPayload: " + hashedRequestPayload);

        String signOrg = timestamp + "\n" + method + "\n" + sb + "\n" + hashedRequestPayload;
        System.out.println(">>> signOrg: " + signOrg);

        byte[] secretSigning = hmac256((sk).getBytes(StandardCharsets.UTF_8), date);
        String signature = DatatypeConverter.printHexBinary(hmac256(secretSigning, signOrg)).toLowerCase();
        System.out.println(">>>> sign: " + signature);

        System.out.println("需要使用的时间戳是：" + timestamp);
        System.out.println("需要使用的sign是：" + signature);

        return signature;
    }

    public static byte[] hmac256(byte[] key, String msg) throws Exception {
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, mac.getAlgorithm());
        mac.init(secretKeySpec);
        return mac.doFinal(msg.getBytes(StandardCharsets.UTF_8));
    }

    public static String sha256Hex(String s) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] d = md.digest(s.getBytes(StandardCharsets.UTF_8));
        return DatatypeConverter.printHexBinary(d).toLowerCase();
    }

    public static void main(String[] args) throws Exception {
//        String body = "{\"departmentId\":11,\"devIds\":[161],\"dlId\":222,\"networkEnv\":1,\"pmId\":160,\"projectAlias\":\"csfcs123\",\"projectName\":\"csfcs123\",\"soId\":174,\"testerIds\":[222]}";
//        createSign(System.currentTimeMillis() / 1000, "POST", new HashMap<String, String>(16) {{
//            put("projectId", "4");
//        }}, "", "b72eac55cdd3435ca0e7c5e54a4cea7d");
    }
}
