package cn.cjd.springboot.demo.ret;

import java.io.Serializable;

/**
 * project  dsmp_jenkins_plugin
 * description: module
 *
 * @author chengjiade
 * date:    2021/10/28 4:48 下午
 * @version v1.0
 */
public class ResponseResultVo<T> implements Serializable {
    private static final long serialVersionUID = 5164792769547664759L;

    public static final Integer SUCCESS_CODE = 2000;

    /**
     * 请求唯一Id
     **/
    private String requestId;

    /**
     * 错误码
     **/
    private int code;

    /**
     * 错误信息
     **/
    private String message;

    /**
     * 返回值内容
     **/
    private T data;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
