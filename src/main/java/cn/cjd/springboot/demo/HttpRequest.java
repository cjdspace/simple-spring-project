package cn.cjd.springboot.demo;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.ssl.SSLContextBuilder;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

/**
 * @author chengjiade
 * http请求
 */
public class HttpRequest {

    private HttpClientContext httpClientContext;

    private SSLConnectionSocketFactory sslFactory;

    public SSLConnectionSocketFactory getSslFactory() {
        return sslFactory;
    }

    public HttpClientContext getHttpClientContext() {
        return httpClientContext;
    }

    public HttpRequest() {
        setContext();
        try {
            setSslFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setContext() {
        HttpClientContext httpClientContext = HttpClientContext.create();
        RequestConfig config = RequestConfig.custom()
                .setMaxRedirects(3)
                .setConnectionRequestTimeout(5000)
                .setConnectTimeout(5000)
                .build();

        httpClientContext.setRequestConfig(config);

        this.httpClientContext = httpClientContext;

        Properties properties = System.getProperties();
        properties.setProperty("proxySet", "false");
        properties.setProperty("socksProxyHost", "");
        properties.setProperty("socksProxyPort", "");
    }

    public void setSslFactory() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        // use the TrustSelfSignedStrategy to allow Self Signed Certificates
        SSLContext sslContext = SSLContextBuilder.create()
                .loadTrustMaterial(new TrustSelfSignedStrategy())
                .build();
        // we can optionally disable hostname verification.
        // if you don't want to further weaken the security, you don't have to include this.
        HostnameVerifier allowAllHosts = new NoopHostnameVerifier();

        // create an SSL Socket Factory to use the SSLContext with the trust self signed certificate strategy
        // and allow all hosts verifier.
        this.sslFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);
    }
}
