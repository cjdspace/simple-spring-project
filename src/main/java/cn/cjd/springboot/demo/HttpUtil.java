package cn.cjd.springboot.demo;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * project  DSMP
 * description: module
 *
 * @author chengjiade
 * date:    2022/2/9 11:30 上午
 * @version v1.0
 */
public class HttpUtil {

    private final HttpRequest httpRequestUtil = new HttpRequest();
    private final static String GET = "GET";
    private final static String POST = "POST";

    public String host;
    public String accessKey;
    public String secretKey;

    public HttpUtil(String host, String accessKey, String secretKey) {
        this.host = host;
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        formatHost();
    }
    protected void formatHost() {
        if (host.charAt(host.length() - 1) == '/') {
            host = host.substring(0, host.length() - 1);
        }
    }

    public String get(String path, List<NameValuePair> args) throws Exception {
        Map<String, String> params = new HashMap<>(16);
        URI uri;
        if (args != null) {
            uri = new URIBuilder(host + path).addParameters(args).build();
            for (NameValuePair arg : args) {
                params.put(arg.getName(), arg.getValue());
            }
        } else {
            uri = new URIBuilder(host + path).build();
        }

        HttpGet httpGet = new HttpGet(uri);
        for (Header header : getHeaders(GET, params, "")) {
            httpGet.addHeader(header);
        }

        CloseableHttpClient client = HttpClients.custom().setSSLSocketFactory(httpRequestUtil.getSslFactory()).build();

        CloseableHttpResponse response = client.execute(httpGet, httpRequestUtil.getHttpClientContext());
        HttpEntity e = response.getEntity();

        return EntityUtils.toString(e, StandardCharsets.UTF_8);
    }

    public String post(String path, String data) throws Exception {
        CloseableHttpClient client = HttpClients.custom().setSSLSocketFactory(httpRequestUtil.getSslFactory()).build();

        HttpPost httpPost = new HttpPost(host + path);

        httpPost.addHeader("Content-Type", "application/json;charset=utf-8");
        //noinspection AlibabaUndefineMagicConstant
        for (Header header : getHeaders(POST, new HashMap<>(16), data)) {
            httpPost.addHeader(header);
        }

        httpPost.setEntity(new StringEntity(data, StandardCharsets.UTF_8));
        CloseableHttpResponse response = client.execute(httpPost, httpRequestUtil.getHttpClientContext());
        HttpEntity res = response.getEntity();

        return EntityUtils.toString(res, StandardCharsets.UTF_8);
    }

    private List<Header> getHeaders(String method, Map<String, String> params, String body) {
        long timestamp = System.currentTimeMillis() / 1000;
        String sign = null;
        try {
            sign = SignUtil.createSign(timestamp, method, params, body, secretKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] key = {"timestamp", "ver", "signMethod", "nonce", "sign", "accessKey", "dryRun"};
        String[] value = {String.valueOf(timestamp), "1.0", "HMAC-SHA256", UUID.randomUUID().toString().replaceAll("-",""), sign, accessKey, "0"};
        List<Header> headers = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            headers.add(new BasicHeader(key[i], value[i]));
        }
        return headers;
    }

}
