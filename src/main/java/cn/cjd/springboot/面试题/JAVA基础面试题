1、访问修饰符public、private、protect、default范围
    public： Java语言中访问限制最宽的修饰符，一般称之为“公共的”。被其修饰的类、属性以及方法不仅可以跨类访问，而且允许跨包（package）访问。
    private: Java语言中对访问权限限制的最窄的修饰符，一般称之为“私有的”。被其修饰的类、属性以 及方法只能被该类的对象访问，其子类不能访问，更不能允许跨包访问。
    protect: 介于public 和 private 之间的一种访问修饰符，一般称之为“保护形”。被其修饰的类、属性以及方法只能被类本身的方法及子类访问，即使子类在不同的包中也可以访问。
    default：即不加任何访问修饰符，通常称为“默认访问模式“。该模式下，只允许在同一个包中进行访问。
                    同一个类        同一个包       不同包的子类     不同包的非子类
    Private           √
    Default           √               √
    Protected         √               √               √
    Public            √               √               √                √


2、理解final
    final修饰类：   被final修饰的类不能被继承，即它不能拥有自己的子类
    final修饰方法：  被final修饰的方法不能被重写
    final修饰变量：  final修饰的变量，无论是类属性、对象属性、形参还是局部变量，都需要进行初始化操作


3. String类为什么是final修饰的
    1.为了实现字符串池
    1、线程安全：      由于String类不能被继承，所以就不会没修改，这就避免了因为继承引起的安全隐患
    2、操作系统安全：   JAVA好多方法都是调用的操作系统的API，这就是著名的"本地方法调用"，都是非常底层的，如果可以被继承然后被重写了，则会引起系统的安全隐患
    3.为了实现String可以创建HashCode不可变性
    4、提高效率：      String类在程序中出现的频率比较高，如果为了避免安全隐患，在它每次出现时都用final来修饰，这无疑会降低程序的执行效率，所以干脆直接将其设为final一提高效率


4、HashMap的源码，实现原理，底层结构

    数据结构：基于数组和链表来实现的，数据的每个位置都可以存放链表，通过计算散列码来决定存储的位置

    源码分析、底层结构：
        implements Map, 每一个数据都是Node<k,v> implements Map.Entry<k,v>
        维护了Node<k,v>的单向链表结构和Node<k,v>的红黑树结构
        默认初始容量16，默认加载因子0.75，默认碰撞值为8转红黑树结构
        支持4种构造函数HashMap(int initialCapacity, float loadFactor)、HashMap(int initialCapacity)、HashMap()、HashMap(Map<!--? extends K, ? extends V--> m)
        hash值计算方式：通过key的hashcode与key的hashcode右移16未的与逻辑得到
        getValue值：先比较key的hash值，然后通过key和hash位的Node<k,v>链表中比较key去取value
        putValue值：先计算key的hash值，判断table中是否有这个hash值的链表，如果没有new一个Node<k,v>存放，如果存在，则在table的hash值位置的头部添加新的Node，它的next就是原来旧的Node头部
                    当table中hash值的链表个数>=8时改为由原来的单向链表改为红黑树结构
        扩容机制resize()：构造hash表时，如果不指明初始大小，默认大小为16（即Node数组大小16），如果Node[]数组中的元素达到（填充比*Node.length）重新调整HashMap大小 变为原来2倍大小


5、说说你知道的几个Java集合类：list、set、queue、map实现类。。。
    Collection:
        List
            ArrayList
            LinkedList
            Vector
                Stack
            JUC中：
                CopyOnWriteArrayList
        Set
            HashSet:
                LinkedHashSet:
            SortedSet
                TreeSet
            EnumSet
            JUC中：
                CopyOnWriteArraySet
        Map:
            HashMap
                LinkedHashMap
            HashTable
                Properties
            SortedMap
                TreeMap
            WeakHashMap
            IdentityHashMap
            EnumMap
            JUC中：
                ConcurrentHashMap
                ConcurrentSkipListMap
                ConcurrentSkipListSet
        Queue:
            PriorityQueue
            Deque
                ArrayDeque
                LinkedList
            JUC中：
                ArrayBlockingQueue数组有界的阻塞队列
                LinkedBlockingQueue单向链表有界的阻塞队列
                LinkedBlockingDeque双向链表有界的阻塞队列
                ConcurrentLinkedQueue单向链表无界队列
                ConcurrentLinkedDeque双向链表无界队列


6、描述一下ArrayList和LinkedList各自实现和区别
    实现：
        1.List是接口类，ArrayList和LinkedList是List的实现类。
        2.ArrayList是动态数组（顺序表）的数据结构。顺序表的存储地址是连续的，所以在查找比较快，但是在插入和删除时，由于需要把其它的元素顺序向后移动（或向前移动），所以比较熬时。
        3.LinkedList是链表的数据结构。链表的存储地址是不连续的，每个存储地址通过指针指向，在查找时需要进行通过指针遍历元素，所以在查找时比较慢。由于链表插入时不需移动其它元素，所以在插入和删除时比较快。
    区别：
        1.ArrayList是实现了基于动态数组的数据结构，LinkedList基于链表的数据结构。
        2.对于随机访问get和set，ArrayList觉得优于LinkedList，因为LinkedList要移动指针。
        3.对于新增和删除操作add和remove，LinedList比较占优势，因为ArrayList要移动数据。


7、Java中的队列都有哪些，有什么区别
    阻塞队列：
        ArrayBlockingQueue： 一个由数组支持的有界队列。
        LinkedBlockingQueue：一个由链接节点支持的可选有界队列。
        PriorityBlockingQueue ：一个由优先级堆支持的无界优先级队列。
        DelayQueue：一个由优先级堆支持的、基于时间的调度队列。
        SynchronousQueue：一个利用 BlockingQueue 接口的简单聚集（synchronous）机制。
    非阻塞队列：
        PriorityQueue： 类实质上维护了一个有序列表。加入到 Queue 中的元素根据它们的天然排序（通过其 java.util.Comparable 实现）或者根据传递给构造函数的 java.util.Comparator 实现来定位。
        ConcurrentLinkedQueue：是基于链接节点的、线程安全的队列。并发访问不需要同步。因为它在队列的尾部添加元素并从头部删除它们，所以只要不需要知道队列的大小，ConcurrentLinkedQueue 对公共集合的共享访问就可以工作得很好。收集关于队列大小的信息会很慢，需要遍历队列。


8、反射中，Class.forName和classloader的区别
    类的装载过程为： 加载--->验证--->准备--->解析--->初始化--->使用--->卸载
                         |___________________|
                                  链接

    加载：通过类的全限定名获取二进制字节流，将二进制字节流转换成方法区中的运行数据结构，内存中生成Java.lang.class对象
    链接：
        验证：检查导入类或接口的二进制数据的正确性
        准备：给类的静态变量分配并初始化存储空间；初始值指的是类变量类型的默认值而不是实际要赋的值
        解析：将常量池中的符号引用转成直接引用
    初始化：静态变量的初始化和静态Java代码块，并初始化程序设置的变量值。

    Class.forName(className)方法：
        内部实际调用的方法是  Class.forName(className,true,classloader);
        第2个boolean参数表示类是否需要初始化，  Class.forName(className)默认是需要初始化。
        一旦初始化，就会触发目标对象的 static块代码执行，static参数也也会被再次初始化。

    ClassLoader.loadClass(className)方法：
        内部实际调用的方法是  ClassLoader.loadClass(className,false);
        第2个 boolean参数，表示目标对象是否进行链接，false表示不进行链接，由上面介绍可以，
        不进行链接意味着不进行包括初始化等一些列步骤，那么静态块和静态对象就不会得到执行


9. Java5、Java6、Java7、Java8的新特性(baidu问的,好BT)
    Java5:
        1、泛型 Generics
        2、枚举类型 Enumeration
        3、自动装箱拆箱（自动类型包装和解包）autoboxing & unboxing
        4、可变参数varargs(varargs number of arguments)
        5、Annotations 它是java中的metadata（注释）
        6、新的迭代语句（for(int n:numbers)）
        7、静态导入（import static ）
        8、新的格式化方法（java.util.Formatter）
        9、新的线程模型和并发库Thread Framework
    Java6:
        1、引入了一个支持脚本引擎的新框架
        2、UI的增强
        3、对WebService支持的增强（JAX-WS2.0和JAXB2.0）
        4、一系列新的安全相关的增强
        5、JDBC4.0
        6、Compiler API
        7、通用的Annotations支持
    Java7:
        1，switch中可以使用字串了
        2.运用List<String> tempList = new ArrayList<>(); 即泛型实例化类型自动推断
        3.语法上支持集合，而不一定是数组（final List<Integer> piDigits = [1,2,3,4,5,8]; ）
        4.新增一些取环境信息的工具方法
        5.Boolean类型反转，空指针安全,参与位运算
        6.两个char间的equals
        7.安全的加减乘除 （Math.safeToInt(long value)）
        8.map集合支持并发请求，且可以写成 Map map = {name:"xxx",age:18};
    Java8:
        1.接口的默认方法
        2.Lambda 表达式
        3.函数式接口
        4.方法与构造函数引用
        5.Lambda 作用域
        6.访问局部变量
        7.访问对象字段与静态变量
        8.访问接口的默认方法
        9.Date API
        10.Annotation 注解


10. Java数组和链表两种结构的操作效率，在哪些情况下(从开头开始，从结尾开始，从中间开始)，哪些操作(插入，查找，删除)的效率高





11、session和cookie的区别和联系，session的生命周期，多个服务部署时session管理
    session：
        session的中文翻译是“会话”，当用户打开某个web应用时，便与web服务器产生一次session。
        服务器使用session把用户的信息临时保存在了服务器上，用户离开网站后session会被销毁。
        这种用户信息存储方式相对cookie来说更安全，可是session有一个缺陷：如果web服务器做了负载均衡，那么下一个操作请求到了另一台服务器的时候session会丢失。
    cookie：
        cookie是保存在本地终端的数据。cookie由服务器生成，发送给浏览器，浏览器把cookie以kv形式保存到某个目录下的文本文件内，下一次请求同一网站时会把该cookie发送给服务器。
        由于cookie是存在客户端上的，所以浏览器加入了一些限制确保cookie不会被恶意使用，同时不会占据太多磁盘空间，所以每个域的cookie数量是有限的。
        cookie的组成有：名称(key)、值(value)、有效域(domain)、路径(域的路径，一般设置为全局:"\")、失效时间、安全标志(指定后，cookie只有在使用SSL连接时才发送到服务器(https))。
        下面是一个简单的js使用cookie的例子:
            用户登录时产生cookie:
                document.cookie = "id="+result.data['id']+"; path=/";
                document.cookie = "name="+result.data['name']+"; path=/";
                document.cookie = "avatar="+result.data['avatar']+"; path=/";
            使用到cookie时做如下解析：
                var cookie = document.cookie;
                var cookieArr = cookie.split(";");
                var user_info = {};
                for(var i = 0; i < cookieArr.length; i++) {
                    user_info[cookieArr[i].split("=")[0]] = cookieArr[i].split("=")[1];
                }
                $('#user_name').text(user_info[' name']);
                $('#user_avatar').attr("src", user_info[' avatar']);
                $('#user_id').val(user_info[' id']);
    token：
        token的意思是“令牌”，是用户身份的验证方式，最简单的token组成:uid(用户唯一的身份标识)、time(当前时间的时间戳)、sign(签名，由token的前几位+盐以哈希算法压缩成一定长的十六进制字符串，可以防止恶意第三方拼接token请求服务器)。还可以把不变的参数也放进token，避免多次查库

    cookie 和session的区别：
        1、cookie数据存放在客户的浏览器上，session数据放在服务器上。
        2、cookie不是很安全，别人可以分析存放在本地的cookie并进行cookie欺骗，考虑到安全应当使用session。
        3、session会在一定时间内保存在服务器上。当访问增多，会比较占用你服务器的性能，考虑到减轻服务器性能方面，应当使用cookie。
        4、单个cookie保存的数据不能超过4K，很多浏览器都限制一个站点最多保存20个cookie。
        5、所以个人建议：将登陆信息等重要信息存放为session，其他信息如果需要保留，可以放在cookie中


12、Java的内存模型以及GC算法
