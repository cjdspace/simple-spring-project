package cn.cjd.springboot.noScan;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 测试Spring 注入
 * Created by 170096 on 2018/9/11.
 */
public class TestSpringXml {
    public TestSpringXml() {
        System.out.println("这是一个不在 spring boot 扫描范围内的测试类");
    }

    public static void main(String[] args) {
        String orderInfoList = "[{\"businessId\":\"1eac95dc5d7e4f5a83368f85951c4af5\",\"businessType\":\"DEL\",\"payPurposeType\":\"REN\",\"financeType\":\"PAY\",\"chargRuleId\":\"04c3da60d4af4b04934dcc9f10c67349\",\"payDurationStartTime\":1582992000000,\"payDurationEndTime\":1585584000000,\"estimatedPayTime\":1582992000000,\"amount\":1708,\"periodicityName\":0,\"advanceDays\":0,\"_amount\":1708,\"payPurposeTypeText\":\"租金\",\"__payDurationStartTime\":1582992000000,\"__payDurationEndTime\":1585584000000,\"__advanceDays\":0,\"__amount\":3416000,\"html\":\"<div class=\\\"table-field-row\\\"><div class=\\\"_pair\\\"><span style=\\\"\\\">2020-03-01</span> 至 <span style=\\\"\\\">2020-03-31</span></div><div class=\\\"_pair\\\"><span>提前付款天数：</span><span style=\\\"\\\">0</span></div><div class=\\\"_pair\\\"><span>租金：</span><span style=\\\"color:red;\\\">1708.00元</span></div></div>\"}]";
        String regex = "[(A-Za-z)<>=/_:]";
        JSONArray jsonArray = JSON.parseArray(orderInfoList);
        StringBuilder orderSB = new StringBuilder();
        for (Object object : jsonArray) {
            JSONObject order = JSONObject.parseObject(object.toString());
            String content = String.valueOf(order.get("html")).replaceAll(regex, "")
                    .replaceAll("\\\\", "")
                    .replaceAll("\"", "")
                    .replaceAll(";","")
                    .replaceAll(" ", "")
                    .replaceAll("--","") + "; ";
            orderSB.append(content);
        }
        System.out.println(orderSB.toString());
    }
}
