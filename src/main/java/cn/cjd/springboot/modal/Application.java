package cn.cjd.springboot.modal;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

//@EnableHystrix
//@EnableHystrixDashboard
@ComponentScan({"cn.cjd.springboot.modal"})
@EnableCaching //开启缓存
@EnableTransactionManagement // 开启事务管理
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@ImportResource(locations = {"classpath:/xml/application-bean.xml"})
@EnableSwagger2Doc
public class Application {

    public static void main( String[] args ){
        SpringApplication springApplication = new SpringApplication(Application.class);
        springApplication.run(args);
    }

    @Bean("defaultStart")
    public CommandLineRunner defaultStart() {
        return (String... strings) -> {
            System.out.println("项目启动完毕，初始化执行的方法");
        };
    }

    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}

