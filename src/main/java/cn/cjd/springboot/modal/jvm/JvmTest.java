package cn.cjd.springboot.modal.jvm;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Api(value = "/jvm", description="JVM接口")
@RestController
@RequestMapping("/jvm")
public class JvmTest {
    private static List<String> list = new ArrayList<>();

    @ApiOperation(value = "/dead",notes = "让项目堆溢出死掉")
    @GetMapping(value = "/dead")
    public Object testQueryDate(){
        while (true) {
            long i = 1;
            list.add("dafasdfadsfgagdsaghnwruhweinfjdnajdasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfjdnarwerqjdasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfeqwerjdnajdasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfjdnewrwerdfsajdasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfjdnajcxzvxdasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfjdnavxvfsdfjdasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfjrewqrdnajdasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfjdnajzzsddasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfjdnajzzsddasfljahweqrsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinfjqwerdnajzzsddasfljahsfdsa" + i);
            list.add("dafasdwqrfadsfgagdsaghnwruhweinfjdnajzzsddasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwrurewqrhweinfjdnajzzsddasfljahsfdsa" + i);
            list.add("dafasdfadsfgagdsaghnwruhweinewqrfjdnajzzsddasfljahsfdsa" + i);
            i++;
        }
    }
}
