package cn.cjd.springboot.modal.springUse.springContext;

import org.springframework.stereotype.Component;

@Component
public class TestSpringContextBean {

    public void sayIGet(){
        System.out.println("you get me");
    }
}
