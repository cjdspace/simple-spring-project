package cn.cjd.springboot.modal.config.interceptor;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
public class TwoInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        log.info("----------------------->进入拦截器two");
//        log.error("被two拦截。。。");
//        returnErrorMsg(httpServletResponse, new JSONObject(){{
//            put("code", "500");
//            put("message", "被two拦截。。。");
//        }});
        log.info("----------------------->出拦截器two");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }

    public static void returnErrorMsg(HttpServletResponse response, JSON json) throws Exception{
        OutputStream out = null;
        try {
            response.setCharacterEncoding("utf-8");
            response.setContentType("text/json");
            out = response.getOutputStream();
            out.write(JSON.toJSONString(json).getBytes(StandardCharsets.UTF_8));
            out.flush();
        } finally {
            if (out != null){
                out.close();
            }
        }
    }
}
