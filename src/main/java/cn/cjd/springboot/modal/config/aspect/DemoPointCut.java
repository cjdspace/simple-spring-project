package cn.cjd.springboot.modal.config.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Aspect
@Component
public class DemoPointCut {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebLogAspect.class);
    private ThreadLocal<DemoPointCut.HttpLog> log = new ThreadLocal<>();

    //    @Pointcut("execution(* cn.cjd.springboot.modal.springUse.simple_orm..*(..))")
    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    private void pointCut(){
    }

    @Before("pointCut()")
    public void before() {
        log.get().setHandel("before: 方法处理前");
    }

    @After("pointCut()")
    public void after() {
        log.get().setHandel("after: 方法处理后");
    }

    @SuppressWarnings("unused")
    @AfterReturning(pointcut = "pointCut()", returning = "returnObject")
    public void afterReturning(JoinPoint joinPoint, Object returnObject) {
        log.get().setHandel("afterReturning: 返回结果，这个时候客户端还没有拿到结果");
        LOGGER.info(JSON.toJSONString(log.get().getHandList(), SerializerFeature.PrettyFormat));
    }

    @AfterThrowing("pointCut()")
    public void afterThrowing() {
        log.get().setHandel("afterThrowing: 异常情况 rollback");
    }

    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            log.set(new HttpLog());
            log.get().setHandel("around: 先于before处理");
            return joinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        } finally {
            log.get().setHandel("around: 方法处理完毕，但是先于after执行");
        }
    }


    public static class HttpLog {

        private List<String> handList = new ArrayList<>();

        void setHandel(String handel) {
            handList.add(handel);
        }

        List<String> getHandList() {
            return handList;
        }
    }
}
