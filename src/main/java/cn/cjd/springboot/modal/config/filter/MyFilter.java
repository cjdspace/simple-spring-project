package cn.cjd.springboot.modal.config.filter;

import cn.cjd.springboot.modal.common.utils.simpleUtils.HttpHeaderUtil;
import cn.cjd.springboot.modal.config.interceptor.TwoInterceptor;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

@Slf4j
@SuppressWarnings({"unused", "RedundantThrows"})
public class MyFilter implements Filter {

    private final static Logger logger = LoggerFactory.getLogger(MyFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
        System.out.println("----------------------->过滤器被创建");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("----------------------->进入过滤器");
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        String requestURI = req.getRequestURI();
        logger.info("IP={}, URL={}, METHOD={}", req.getRemoteAddr(), req.getRequestURI(), req.getMethod());
//        logger.info("requestURI={}", requestURI);
//        logger.info("request={}", req);
        if (requestURI.contains("/test")) {
            filerTestInterface(servletRequest, servletResponse, filterChain);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        log.info("----------------------->过滤器处理完毕");
    }

    private void filerTestInterface(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            TwoInterceptor.returnErrorMsg(Objects.requireNonNull(HttpHeaderUtil.getResponse()), new JSONObject() {{
                put("code", "500");
                put("message", "测试接口不允许使用");
            }});
        } catch (Exception e) {
            e.printStackTrace();
        }
//        servletRequest.getRequestDispatcher("/test/refused").forward(servletRequest, servletResponse);
//        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        System.out.println("----------------------->过滤器被销毁");
    }
}
