package cn.cjd.springboot.modal.config.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by 170096 on 2017/8/14
 */
@Aspect
@Component
public class HttpAspect {

    private final static Logger logger = LoggerFactory.getLogger(HttpAspect.class);

    //    @Pointcut("execution(public * cn.cjd.springboot.modal.common.fileServer.web.Ctrl..*(..))")
    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void log() {
    }

    @Before("log()")
    public void doBefore(JoinPoint joinPoint) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Map<String, String> map = new HashMap<String, String>(){{
            //url
            put("url", request.getRequestURI());
            //method
            put("method", request.getMethod());
            //IP
            put("IP", request.getRemoteAddr());
            //类方法
            put("class_method", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
            //参数
            put("args", JSON.toJSONString(joinPoint.getArgs()));
        }};
        logger.info("HttpInfo: {}", JSON.toJSONString(map, SerializerFeature.PrettyFormat));
    }

    @After("log()")
    public void doAfter() {
        logger.info("after");
    }

    @AfterReturning(returning = "object", pointcut = "log()")
    public void doAfterReturning(Object object) {
        if (!Objects.isNull(object)) {
            logger.info("response={}", object.toString());
        }
    }
}
