package cn.cjd.springboot.modal.config.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class OneInterceptor implements HandlerInterceptor {

    /**
     * 再请求处理之前被调用，再渲染视图之前（Controller之后）
     */
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        log.info("----------------------->进入拦截器One");
//        log.info("被 One 拦截。。。 放行通过");
        log.info("----------------------->出拦截器One");
        return true;
    }

    /**
     * 再请求结束之后被调用，也就是DispatcherServlet渲染了对应视图之后执行
     */
    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
