package cn.cjd.springboot.modal.config.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.nutz.lang.Lang;
import org.nutz.lang.random.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class WebLogAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebLogAspect.class);
    private ThreadLocal<Long> startTime = new ThreadLocal<>();
    private ThreadLocal<WebLogAspect.HttpLog> httpLog = new ThreadLocal<>();

    public WebLogAspect() {
    }

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void webLog() {
    }

    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        this.startTime.set(System.currentTimeMillis());
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (!Lang.isEmpty(attributes)) {
            HttpServletRequest request = attributes.getRequest();
            WebLogAspect.HttpLog log = new WebLogAspect.HttpLog();
            log.setId(R.UU32().toLowerCase());
            log.setUrl(request.getRequestURL().toString());
            log.setIp(Lang.getIP(request));
            log.setClass_method(request.getMethod());
            log.setArgs(joinPoint.getArgs());
            log.setClass_method(joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
            this.httpLog.set(log);
        }
    }

    @Around("webLog()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        try {
            Object obj = proceedingJoinPoint.proceed();
            return obj;
        } catch (Throwable var4) {
            throw var4;
        }
    }

    @AfterReturning(
            returning = "ret",
            pointcut = "webLog()"
    )
    public void doAfterReturning(Object ret) {
        WebLogAspect.HttpLog log = this.httpLog.get();
        if (!Lang.isEmpty(log)) {
            log.setResponse(ret);
            log.setSpend_time(System.currentTimeMillis() - this.startTime.get() + "");
            LOGGER.info("{\n\tid={}\t\tip={}\t\turl={}\t\tclass_method={}\t\tspend_time={}\n\targs={}\n\tresponse={}\n\t}",
                    log.getId(), log.getIp(), log.getUrl(), log.getClass_method(), log.getSpend_time(), JSON.toJSONString(log.getArgs(), SerializerFeature.PrettyFormat), JSON.toJSONString(log.getResponse(), SerializerFeature.PrettyFormat));
            LOGGER.info(JSON.toJSONString(log, SerializerFeature.PrettyFormat));
        }
    }

    public static class HttpLog {
        private String id;
        private String ip;
        private String localhost;
        private String url;
        private String http_method;
        private String class_method;
        private String spend_time;
        private Object[] args;
        private Object response;

        public HttpLog() {
        }

        public String getId() {
            return this.id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getHttp_method() {
            return this.http_method;
        }

        public void setHttp_method(String http_method) {
            this.http_method = http_method;
        }

        public String getIp() {
            return this.ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getClass_method() {
            return this.class_method;
        }

        public void setClass_method(String class_method) {
            this.class_method = class_method;
        }

        public Object[] getArgs() {
            return this.args;
        }

        public void setArgs(Object[] args) {
            this.args = args;
        }

        public Object getResponse() {
            return this.response;
        }

        public void setResponse(Object response) {
            this.response = response;
        }

        public String getSpend_time() {
            return this.spend_time;
        }

        public void setSpend_time(String spend_time) {
            this.spend_time = spend_time;
        }

        public String getLocalhost() {
            return this.localhost;
        }

        public void setLocalhost(String localhost) {
            this.localhost = localhost;
        }
    }
}
