package cn.cjd.springboot.modal.common.utils.xmlUtils;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class ParseXml {

    public static void main(String[] args) throws Exception {
        File file = new File("/Users/chengjiade/Desktop/simple-spring-project/src/main/java/cn/cjd/springboot/modal/common/utils/xmlUtils/aaa.xml");
        DOMParse(file);
        DOM4JParse();
    }

    private static void DOMParse(File file) throws Exception {
        System.out.println("=====================DOMParse============================");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);
        Node node = document.getElementsByTagName("MessageHeader").item(0);
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node child = childNodes.item(i);
            // 区分element节点与text节点
            if (Node.ELEMENT_NODE == child.getNodeType()) {
                System.out.print(child.getNodeName() + ": ");
                System.out.println(child.getTextContent());
            }
        }
    }

    private static final String XML = "<ESBEntry><MessageHeader><Fid>BS10003</Fid><SourceSysCode>S00</SourceSysCode><TargetSysCode>S68</TargetSysCode><MsgDate>2020-12-10 15:01:10.049</MsgDate></MessageHeader><RetInfo><RetCode>1</RetCode><RetCon>query success</RetCon></RetInfo><ResponseOption><currentPage>1</currentPage><totalPage>1</totalPage></ResponseOption><MsgInfo><Msg><![CDATA[<msg><body><row action='select'><PAT_NAME>刘浴娣</PAT_NAME><PHYSI_SEX_CODE>2</PHYSI_SEX_CODE><PHYSI_SEX_NAME>女</PHYSI_SEX_NAME><INHOSP_INDEX_NO>00401007</INHOSP_INDEX_NO><MR_NO>00401007</MR_NO><NURSING_LEVEL_CODE>2</NURSING_LEVEL_CODE><ADMIT_WARD_CODE>615</ADMIT_WARD_CODE><ADMIT_WARD_NAME/><ADMIT_BED_CODE>3039</ADMIT_BED_CODE><ADMIT_BED_NAME/><UPDATE_DATE/><INHOSP_FLAG>1</INHOSP_FLAG></row></body></msg>]]></Msg><Msg><![CDATA[<msg><body><row action='select'><PAT_NAME>张三</PAT_NAME><PHYSI_SEX_CODE>2</PHYSI_SEX_CODE><PHYSI_SEX_NAME>女</PHYSI_SEX_NAME><INHOSP_INDEX_NO>00401008</INHOSP_INDEX_NO><MR_NO>00401007</MR_NO><NURSING_LEVEL_CODE>2</NURSING_LEVEL_CODE><ADMIT_WARD_CODE>615</ADMIT_WARD_CODE><ADMIT_WARD_NAME/><ADMIT_BED_CODE>3040</ADMIT_BED_CODE><ADMIT_BED_NAME/><UPDATE_DATE/><INHOSP_FLAG>1</INHOSP_FLAG></row></body></msg>]]></Msg></MsgInfo></ESBEntry>";

    private static void DOM4JParse() throws Exception {
        System.out.println("=====================DOM4JParse============================");
        org.dom4j.Document document = DocumentHelper.parseText(ParseXml.XML);
        Element ele = (Element)document.selectSingleNode("/ESBEntry/MsgInfo");
        for (Element element : ele.elements()) {
            String msg = element.getData().toString();
            org.dom4j.Document msgDoc = DocumentHelper.parseText(msg);
            Element element1 = (Element) msgDoc.selectSingleNode("/msg/body/row");
            System.out.print("PAT_NAME: " + element1.elementText("PAT_NAME"));
            System.out.print(", PHYSI_SEX_CODE: " + element1.elementText("PHYSI_SEX_CODE"));
            System.out.print(", PHYSI_SEX_NAME: " + element1.elementText("PHYSI_SEX_NAME"));
            System.out.print(", INHOSP_INDEX_NO: " + element1.elementText("INHOSP_INDEX_NO"));
            System.out.print(", MR_NO: " + element1.elementText("MR_NO"));
            System.out.print(", NURSING_LEVEL_CODE: " + element1.elementText("NURSING_LEVEL_CODE"));
            System.out.print(", ADMIT_WARD_CODE: " + element1.elementText("ADMIT_WARD_CODE"));
            System.out.print(", ADMIT_BED_CODE: " + element1.elementText("ADMIT_BED_CODE"));
            System.out.println(", INHOSP_FLAG: " + element1.elementText("INHOSP_FLAG"));
        }

    }
}
