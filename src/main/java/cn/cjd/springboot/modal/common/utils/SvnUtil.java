package cn.cjd.springboot.modal.common.utils;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.tmatesoft.svn.core.*;
import org.tmatesoft.svn.core.auth.*;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.*;

import java.io.*;
import java.util.*;

/**
 * project  dsmp
 * description: module
 *
 * @author chengjiade
 * date:    2021/12/23 10:09 上午
 * @version v1.0
 */
@Getter
@Setter
public class SvnUtil {

    private String uri;

    private Integer port;

    private Integer authType;

    private List<String> paths;

    private String userName;

    private String password;

    private String privateKey;

    private String savePath;

    public SvnUtil(String uri, Integer port, Integer authType, List<String> paths, String userName, String password, String privateKey, String savePath) {
        this.uri = uri;
        this.port = port;
        this.authType = authType;
        this.paths = paths;
        this.userName = userName;
        this.password = password;
        this.privateKey = privateKey;
        this.savePath = savePath;
        init();
    }

    private void init() {
        // HTTP、HTTPS网络
        DAVRepositoryFactory.setup();
        // SSH网络
        SVNRepositoryFactoryImpl.setup();
        // file网络
        FSRepositoryFactory.setup();
    }

    private final static String SVN_LOCK = "svn_lock_" + UUID.randomUUID().toString().replace("-", "");

    /**
     * 测试连接
     */
    public Boolean testSvnConnection() {
        synchronized (SVN_LOCK) {
            try {
                SVNURL svnurl = SVNURL.parseURIEncoded(uri);
                SVNRepository repository = SVNRepositoryFactory.create(svnurl);
                ISVNAuthenticationManager authManager = getAuthManager();
                repository.setAuthenticationManager(authManager);
                repository.testConnection();
                return Boolean.TRUE;
            } catch (Exception e) {
                e.printStackTrace();
                return Boolean.FALSE;
            }
        }
    }

    /**
     * 拉取代码
     */
    public void pullCode() throws SVNException, IOException, InterruptedException {
        synchronized (SVN_LOCK) {
            if (uri.endsWith("/")) {
                uri = uri.substring(0, uri.length() - 1);
            }

            if (CollectionUtils.isEmpty(paths)) {
                return;
            }
            String projectName = uri.substring(uri.lastIndexOf("/") + 1);
            for (String path : paths) {
                String uri = this.uri.endsWith("/") ? this.uri + path : this.uri + "/" + path;
                if (uri.endsWith("/")) {
                    uri = uri.substring(0, uri.length() - 1);
                }

                String rootPath = uri.substring(uri.lastIndexOf("/") + 1);
                if (projectName.equals(rootPath)) {
                    rootPath = projectName;
                } else {
                    rootPath = projectName + "/" + rootPath;
                }
                String savePath = this.savePath.endsWith("/") ? this.savePath + rootPath : this.savePath + "/" + rootPath;

                pullOnePath(uri, savePath);
            }
        }
    }

    /**
     * 获取代码下的目录
     */
    @SuppressWarnings("rawtypes")
    public List<String> listDir() throws SVNException, IOException, InterruptedException {
        synchronized (SVN_LOCK) {
            SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(uri));
            // 用户凭证
            if (!CxSourceCodeAuthEnum.NULL.getCode().equals(authType)) {
                ISVNAuthenticationManager authManager = getAuthManager();
                repository.setAuthenticationManager(authManager);
            }
            //获取SVNRepository
            Collection entries = repository.getDir("", -1, null, (Collection) null);
            Iterator iterator = entries.iterator();
            List<String> result = new ArrayList<>();
            while (iterator.hasNext()) {
                SVNDirEntry entry = (SVNDirEntry) iterator.next();
                if (entry.getKind() == SVNNodeKind.DIR) {
                    result.add(entry.getName());
                }
            }
            return result;
        }
    }

    private void pullOnePath(String uri, String savePath) throws IOException, InterruptedException, SVNException {

        CmdUtil.exec("rm -rf " + savePath + " && mkdir -p " + savePath);
        System.out.println("savePath: " + savePath);

        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
        SVNClientManager clientManager;
        // 用户凭证
        if (!CxSourceCodeAuthEnum.NULL.getCode().equals(authType)) {
            ISVNAuthenticationManager auth = getAuthManager();
            clientManager = SVNClientManager.newInstance(options, auth);
        } else {
            clientManager = SVNClientManager.newInstance(options);
        }
        SVNUpdateClient updateClient = clientManager.getUpdateClient();
        updateClient.setIgnoreExternals(false);
        SVNURL svnurl = SVNURL.parseURIEncoded(uri);
        updateClient.doCheckout(svnurl, new File(savePath), SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY, true);
    }

    private ISVNAuthenticationManager getAuthManager() throws IOException, InterruptedException {
        File sshFile = null;
        if (!StringUtils.isEmpty(privateKey)) {
            sshFile = createRsaTempFile();
            userName = null;
        }
        if (StringUtils.isEmpty(password)) {
            password = "";
        }
        File dir = SVNWCUtil.getDefaultConfigurationDirectory();
        return SVNWCUtil.createDefaultAuthenticationManager(dir, userName, password.toCharArray(), sshFile, new char[]{}, true);
    }

    private File createRsaTempFile() throws IOException, InterruptedException {
        String usrHome = System.getProperty("user.home");
        String fileName = usrHome + "/.ssh/id_rsa";
        CmdUtil.exec("rm -rf " + fileName);
        File file = new File(fileName);
        try (FileOutputStream fileOutputStream = new FileOutputStream(file);
             OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
             BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter)) {

            String[] words = privateKey.split("\n");

            for (String word : words) {
                bufferedWriter.write(word);
                bufferedWriter.newLine();
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        System.out.println("filePath: " + file.getAbsolutePath());
        return file;
    }

    public static void main(String[] args) throws SVNException, IOException, InterruptedException {

//        SvnUtil svnUtil = new SvnUtil("svn+ssh://192.168.180.122/project1/moresec-archtype", 3690, CxSourceCodeAuthEnum.AUTH.getCode(),
//                Collections.singletonList("moresec-archtype-core"), "svnuser", "123456", null, "/Users/chengjiade/Downloads/svn/");

        SvnUtil svnUtil = new SvnUtil("svn://192.168.180.52/sca_test", 3690, CxSourceCodeAuthEnum.SSH.getCode(),
                Collections.singletonList("php"), "admin", "hika1b2c3+", null, "/Users/chengjiade/Downloads/svn/");

//        SvnUtil svnUtil = new SvnUtil("svn+ssh://gitee.com/cjdspace/moresec-archtype", 8080, CxSourceCodeAuthEnum.SSH.getCode(),
//                Collections.singletonList("moresec-archtype-core"), null, "", privateKey(), "/Users/chengjiade/Downloads/svn/");

//        boolean b = svnUtil.testConnection1();
//        System.out.println(b);
//
//        List<String> strings = svnUtil.listDir1();
//        System.out.println("dir: " + JSON.toJSONString(strings));
//
//        svnUtil.pullCode1();

        // 测试连接
        boolean b = svnUtil.testSvnConnection();
        System.out.println(b);

        // 列举路径
        List<String> strings = svnUtil.listDir();
        System.out.println("dir: " + JSON.toJSONString(strings));

        // 拉取代码
        svnUtil.pullCode();
    }

    /**
     * PASSWORD SVNPasswordAuthentication login:password authentication (svn://, http://)
     * SSH SVNSSHAuthentication In svn+ssh:// tunneled connections
     * SSL SVNSSLAuthentication In secure https:// connections
     * USERNAME SVNUserNameAuthentication With file:/// protocol, on local machines
     */
    @SuppressWarnings("AlibabaUndefineMagicConstant")
    private BasicAuthenticationManager svnAuth(SVNURL svnurl) throws IOException, InterruptedException {

        if (StringUtils.isEmpty(password)) {
            password = "";
        }
        List<SVNAuthentication> svnAuthentications = new ArrayList<>();

        boolean storageAllowed = true;
        boolean isPartial = true;

        // 普通账号密码形式
        svnAuthentications.add(SVNPasswordAuthentication.newInstance(userName, password.toCharArray(), storageAllowed, svnurl, isPartial));
        // SSH账号密码
        svnAuthentications.add(SVNSSHAuthentication.newInstance(userName, password.toCharArray(), port, storageAllowed, svnurl, isPartial));
        // SSH keyFile
        File rsaTempFile = createRsaTempFile();
        svnAuthentications.add(SVNSSHAuthentication.newInstance(userName, rsaTempFile, new char[]{}, port, storageAllowed, svnurl, isPartial));
        // SSH keyString
        svnAuthentications.add(SVNSSHAuthentication.newInstance(userName, privateKey.toCharArray(), new char[]{}, port, storageAllowed, svnurl, isPartial));
        // SSL
        svnAuthentications.add(SVNSSLAuthentication.newInstance(privateKey.getBytes(), password.toCharArray(), storageAllowed, svnurl, isPartial));
        // file账号
        svnAuthentications.add(SVNUserNameAuthentication.newInstance(userName, false, svnurl, storageAllowed));

//        SVNAuthentication svnAuthentication = null;
//        if (uri.contains("svn://") || uri.contains("http://")) {
//            // 账号
//            svnAuthentication = SVNPasswordAuthentication.newInstance(userName, password.toCharArray(), false, svnurl, false);
//        } else if (uri.contains("svn+ssh://")) {
//            // 账号
//            if (CxSourceCodeAuthEnum.AUTH.getCode().equals(authType)) {
//                svnAuthentication = SVNSSHAuthentication.newInstance(userName, password.toCharArray(), port, false, svnurl, false);
//            // ssh密钥
//            } else if (CxSourceCodeAuthEnum.SSH.getCode().equals(authType)) {
//                svnAuthentication = SVNSSHAuthentication.newInstance(userName, privateKey.toCharArray(), new char[]{}, port, false, svnurl, false);
//            }
//        } else if (uri.contains("https://")) {
//            // 证书
//            svnAuthentication = SVNSSLAuthentication.newInstance(privateKey.getBytes(), password.toCharArray(), false, svnurl, false);
//        } else if (uri.contains("file:///")) {
//            // 账号
//            svnAuthentication = SVNUserNameAuthentication.newInstance(userName, false, svnurl, false);
//        }
//        if (Objects.isNull(svnAuthentication)) {
//            return null;
//        }
        return BasicAuthenticationManager.newInstance(svnAuthentications.toArray(new SVNAuthentication[0]));
    }

    private SVNRepository repository;

    private void initRepository() throws SVNException, IOException, InterruptedException {
        SVNURL svnurl = SVNURL.parseURIEncoded(uri);
        repository = SVNRepositoryFactory.create(svnurl);
        repository.setAuthenticationManager(svnAuth(svnurl));
    }

    private boolean testConnection1() {
        try {
            initRepository();
            repository.testConnection();
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    @SuppressWarnings("rawtypes")
    private List<String> listDir1() {
        List<String> dirs = new ArrayList<>();
        try {
            Collection dir = repository.getDir("", -1, null, (Collection) null);
            for (Object o : dir) {
                SVNDirEntry entry = (SVNDirEntry) o;
                if (entry.getKind() == SVNNodeKind.DIR) {
                    dirs.add(entry.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dirs;
    }

    private void pullCode1() throws SVNException, InterruptedException, IOException {
        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
        SVNURL svnurl = SVNURL.parseURIEncoded(uri);
        SVNClientManager svnClientManager = SVNClientManager.newInstance(options, svnAuth(svnurl));
        SVNUpdateClient updateClient = svnClientManager.getUpdateClient();
        updateClient.doCheckout(svnurl, new File(savePath), SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY, true);
    }



    private static String privateKey() {
        return "-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIICXAIBAAKBgQC0BqGq185SdfMAx8ryC5mIjyKuA/tLNXPV5PaOM9ZAVa1IhV84\n" +
                "LPNmKcrqJ8BOy110UwrJBzkKarEWXrx2LL0l5B/LDtIehllE4JvWZ/7fW4RHcTKp\n" +
                "DfX+WWBPkYgJYetm/O8OM3F4QKWd42RvR4bmbA5qck9ScjMfXKGtfQh5hQIDAQAB\n" +
                "AoGBAIskrQP9NO0rWRz7T3LxYEPaTfWuAibJM7TY+XBDxI+JKRvqagRjSeVm2/1L\n" +
                "qCm+qYblQGrMeFwCxskBhCzau34mtwHx3OdHiPXRW7U/WriuFZqUP50Ya0TFszkY\n" +
                "23uTbp44Rw4dD7HmqSdszULLNMoS9FHeFKcs2eLJ7s/k3AxpAkEA6uBEerz0qKlD\n" +
                "SlaSJyiZLW0Y2/PIy7DxffQgyQNaqn2P+d1ei5mf+ZozNgMx9B4P/X8JvIvqcvBB\n" +
                "/uMuISCWfwJBAMQ3gcNC9McTP2zCoWa7NI7fB6Nvm+ZT0mDvuLQjV5+IhB+jKmTZ\n" +
                "hJK3XrhQIQ1N0rk+mi5OYcserj6dOTkDlfsCQCq3VNz5polSF1HawsPqZ0lSsRW3\n" +
                "lYouCQ1+K8RDHuco5NltgDdlXinX+H5XQGuEWCYjR5MJ4z66IVaI3KfbtGMCQBOc\n" +
                "UJXJXVmALPibwzHXR/t3gvXss7IjcoVJsDYgyiMhq8LVtej6bGuVdBwLtiH0QzlM\n" +
                "bnBuWQvJ9zQ+1ndCSn0CQDq6I5TV6nU1b0PkzMKH9EPnA5rtf8ePA2qG6rL5ehMa\n" +
                "hgySpXZWeV8HO8mP7wwbJ5V9HbIiyPxmR1wi7fNWjc0=\n" +
                "-----END RSA PRIVATE KEY-----";
    }

}

@Getter
enum CxSourceCodeAuthEnum {

    /**
     * 上传代码方式
     * 1、没有；2、凭证（用户名密码）；3、个人令牌；4、SSH
     */
    NULL(1, "没有"),
    AUTH(2, "凭证"),
    TOKEN(3, "个人令牌"),
    SSH(4, "SSH");

    private final Integer code;
    private final String name;

    CxSourceCodeAuthEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }
}

