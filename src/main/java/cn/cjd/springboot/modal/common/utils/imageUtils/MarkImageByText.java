package cn.cjd.springboot.modal.common.utils.imageUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

/**
 * 图片添加水印
 */
@SuppressWarnings("WeakerAccess")
public class MarkImageByText {

    public static void main(String[] args) {
        // 原图位置, 输出图片位置, 水印文字颜色, 水印文字
        markImageByText("/Users/chengjiade/Downloads/QQ20200729-142838@2x.png","/Users/chengjiade/Downloads/new.png", "此图片仅供杭州随寓网络科技有限公司使用", Color.blue, -15, "jpg");
    }

    /**
     * 图片添加水印
     * @param srcImgPath       需要添加水印的图片的路径
     * @param outImgPath       添加水印后图片输出路径
     * @param waterMarkContent 水印的文字
     * @param markContentColor 水印文字的颜色
     * @param degree           旋转角度
     * @param formName         图片后缀
     */
    public static void markImageByText(String srcImgPath, String outImgPath, String waterMarkContent, Color markContentColor, Integer degree, String formName) {
        Graphics2D graphics2D = null;
        try (FileOutputStream outImgStream = new FileOutputStream(outImgPath)){
            // 1、源图片
            File srcImgFile = new File(srcImgPath);
            Image srcImg = ImageIO.read(srcImgFile);
            int srcImgWidth = srcImg.getWidth(null);
            int srcImgHeight = srcImg.getHeight(null);
            // 加水印
            BufferedImage bufImg = new BufferedImage(srcImg.getWidth(null), srcImg.getHeight(null), BufferedImage.TYPE_INT_RGB);
            // 2、得到画笔对象
            graphics2D = bufImg.createGraphics();
            // 3、设置对线段的锯齿状边缘处理
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
            // 4、设置水印旋转
            graphics2D.rotate(Math.toRadians(degree), srcImgWidth / 2.0, srcImgHeight / 2.0);
            // 5、设置水印文字颜色
            graphics2D.setColor(markContentColor);
            // 6、设置水印文字Font
            Font font = new Font("宋体", Font.PLAIN, 16);
            graphics2D.setFont(font);
            // 7、设置水印文字透明度
            graphics2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 0.5f));
            // 8、第一参数->设置的内容，后面两个参数->文字在图片上的坐标位置(x,y)
            int x = srcImgWidth > getWatermarkLength(waterMarkContent, graphics2D) ? (srcImgWidth - getWatermarkLength(waterMarkContent, graphics2D)) / 2 : 0;
            graphics2D.drawString(waterMarkContent, x,  srcImgHeight / 2);
            // 输出图片
            ImageIO.write(bufImg, formName, outImgStream);
            outImgStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != graphics2D) {
                graphics2D.dispose();
            }
        }
    }

    /**
     * 获取水印文字总长度
     * @param waterMarkContent 水印的文字
     * @return 水印文字总长度
     */
    private static int getWatermarkLength(String waterMarkContent, Graphics2D g) {
        return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(), 0, waterMarkContent.length());
    }
}
