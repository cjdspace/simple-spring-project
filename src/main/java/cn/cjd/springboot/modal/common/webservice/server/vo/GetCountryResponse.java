package cn.cjd.springboot.modal.common.webservice.server.vo;

import cn.cjd.springboot.modal.common.webservice.server.WebServiceConfig;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getCountryResponse", namespace = WebServiceConfig.NAMESPACE_URI)
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCountryResponse {

    private Country country;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
