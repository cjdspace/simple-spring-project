package cn.cjd.springboot.modal.common.exception;

public class TestCase {

    private void taskNotFoundException() throws TaskNotFoundException {
        throw new TaskNotFoundException("task not found");
    }

    private void sendMsgException() {
        throw new SendMsgException("SendMsgException");
    }

    private void throwE() throws TaskNotFoundException {
//        taskNotFoundException();
        sendMsgException();
    }

    public void test() {
        try {
            throwE();
        } catch (SendMsgException e) {
            System.out.println(e.getMessage());
        } catch (TaskNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("123");
        }
    }

    public static void main(String[] args) {
        TestCase testCase = new TestCase();
        testCase.test();
    }
}
