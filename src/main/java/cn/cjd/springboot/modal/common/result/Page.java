package cn.cjd.springboot.modal.common.result;

import lombok.Data;

import java.util.List;

@Data
public class Page<T> {
    private Long count;
    private Integer pageSize;
    private Integer pageNum;
    private Integer maxPage;
    private List<T> objList;
}
