package cn.cjd.springboot.modal.common.log;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogAnnotation {

    /**
     * 日志描述
     * @return 日志描述
     */
    String value() default "";

    /**
     * 日志事件类型
     * @return 日志事件类型
     */
    int type() default 0;

    /**
     * 日志模块类型
     * @return 日志模块类型
     */
    int module() default 0;
}
