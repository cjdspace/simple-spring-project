package cn.cjd.springboot.modal.common.exception;

public class SendMsgException extends RuntimeException {

    public SendMsgException(String message) {
        super(message);
    }

}
