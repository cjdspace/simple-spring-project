package cn.cjd.springboot.modal.common.designPatterns.factory;

/**
 * 工厂方法模式
 */
@SuppressWarnings("unused")
public class Factory2 {
    public static void main(String[] arg) {
        AbstractFactory miFactory = new XiaoMiFactory();
        AbstractFactory appleFactory = new AppleFactory();
        MiPhone miPhone = (MiPhone) miFactory.makePhone();
        IPhone iPhone = (IPhone) appleFactory.makePhone();
    }
}

interface AbstractFactory {
    Phone makePhone();
}

class XiaoMiFactory implements AbstractFactory{
    @Override
    public Phone makePhone() {
        return new MiPhone();
    }
}

class AppleFactory implements AbstractFactory {
    @Override
    public Phone makePhone() {
        return new IPhone();
    }
}

class MiPhone implements Phone {
    MiPhone() {
        this.make();
    }
    @Override
    public void make() {
        // TODO Auto-generated method stub
        System.out.println("make xiaomi phone!");
    }
}

class IPhone implements Phone {
    IPhone() {
        this.make();
    }
    @Override
    public void make() {
        // TODO Auto-generated method stub
        System.out.println("make iphone!");
    }
}