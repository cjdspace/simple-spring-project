package cn.cjd.springboot.modal.common.http;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Slf4j
@SuppressWarnings("unused")
public class HttpUtil {

    private final static Integer SOCKET_TIMEOUT = 15000;//读取数据超时
    private final static Integer CONNECT_TIMEOUT = 15000;//链接超时
    private final static String CONTENT_TYPE_JSON = "application/json; charset=UTF-8";
    private final static String CONTENT_TYPE_XML = "text/xml; charset=UTF-8";

    /**
     * httpGet
     */
    public static String httpGet(String url, Map<String, String> headerMap) {
        HttpGet httpGet = createHttpGet(url, headerMap);
        return httpGet(httpGet);
    }

    /**
     * httpPostJSON
     */
    public static String httpPostJson(String url, Map<String, String> headerMap, String postData) {
        HttpPost httpPost = createHttpPost(url, headerMap, CONTENT_TYPE_JSON);
        StringEntity stringEntity = getStringEntity(postData, CONTENT_TYPE_JSON);
        return httpPost(httpPost, stringEntity);
    }

    /**
     * httpPostXml
     */
    public static String httpPostXml(String url, String postData, Map<String, String> headerMap) {
        // 创建请求
        HttpPost httpPost = createHttpPost(url, headerMap, CONTENT_TYPE_XML);
        // 设置请求体
        StringEntity stringEntity = getStringEntity(postData, CONTENT_TYPE_XML);
        return httpPost(httpPost, stringEntity);
    }

//  =======================================以下是私有化方法======================================================

    private static String httpPost(HttpPost httpPost, StringEntity stringEntity) {
        String result = null;
        CloseableHttpClient httpClient = createCloseableHttpClient();
        try {
            httpPost.setEntity(stringEntity);
            //执行请求
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity, StandardCharsets.UTF_8.toString());
                EntityUtils.consume(entity);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return e.getMessage();
        } finally {
            try {
                //释放链接
                httpClient.close();
            } catch (IOException e) {
                log.error("关闭CloseableHttpClient失败, message: {}", e.getMessage());
            }
        }
        return result;
    }

    private static String httpGet(HttpGet httpGet) {
        String result = null;
        CloseableHttpClient httpClient = createCloseableHttpClient();
        try {
            //执行请求
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity, StandardCharsets.UTF_8.toString());
                EntityUtils.consume(entity);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                //释放链接
                httpClient.close();
            } catch (IOException e) {
                log.error("关闭CloseableHttpClient失败, message: {}", e.getMessage());
            }
        }
        return result;
    }

    private static HttpGet createHttpGet(String url, Map<String, String> headerMap) {
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36");
        // 设置参数
        if (!CollectionUtils.isEmpty(headerMap)) {
            for (Map.Entry<String, String> x : headerMap.entrySet()) {
                httpGet.setHeader(x.getKey(), x.getValue());
            }
        }
        return httpGet;
    }

    private static HttpPost createHttpPost(String url, Map<String, String> headerMap, String contentType) {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36");
        httpPost.setHeader("Content-Type", contentType);
        //设置header参数
        if (!CollectionUtils.isEmpty(headerMap)) {
            for (Map.Entry<String, String> x : headerMap.entrySet()) {
                httpPost.setHeader(x.getKey(), x.getValue());
            }
        }
        return httpPost;
    }

    private static StringEntity getStringEntity(String postData, String contentType) {
        StringEntity stringEntity = new StringEntity(postData, StandardCharsets.UTF_8);
        stringEntity.setContentEncoding(StandardCharsets.UTF_8.toString());
        stringEntity.setContentType(contentType);
        return stringEntity;
    }

    private static CloseableHttpClient createCloseableHttpClient() {

        SocketConfig socketConfig = SocketConfig.custom()
                .setSoKeepAlive(false)
                .setSoLinger(1)
                .setSoReuseAddress(true)
                .setSoTimeout(10000)
                .setTcpNoDelay(true)
                .build();

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setSocketTimeout(SOCKET_TIMEOUT)
                .setConnectionRequestTimeout(CONNECT_TIMEOUT)
                .build();

        return HttpClientBuilder.create()
                .setDefaultSocketConfig(socketConfig)
                .setDefaultRequestConfig(requestConfig)
                .build();
    }
}
