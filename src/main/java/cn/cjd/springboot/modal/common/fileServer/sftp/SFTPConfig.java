package cn.cjd.springboot.modal.common.fileServer.sftp;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class SFTPConfig {

    @Value("${sftp.ip}")
    private String ip;

    @Value("${sftp.user}")
    private String user;

    @Value("${sftp.password}")
    private String password;

    @Value("${sftp.port}")
    private Integer port;

    @Value("${sftp.dst}")
    private String dst;

    @Value("${sftp.privateKey}")
    private String privateKey = "";

    @Value("${sftp.passphrase}")
    private String passphrase = "";

    @Value("${sftp.timeout}")
    private Integer timeout;
}
