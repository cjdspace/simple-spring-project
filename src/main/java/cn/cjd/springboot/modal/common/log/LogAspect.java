package cn.cjd.springboot.modal.common.log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
@Aspect
public class LogAspect {

    private final String annotationPointCut = "@annotation(cn.cjd.springboot.modal.common.log.LogAnnotation)";

    @Pointcut(value = annotationPointCut)
    public void logAnnotation() {
    }

    @Around(value = "logAnnotation()")
    public Object aroundAnnotation(ProceedingJoinPoint proceedingJoinPoint) {
        Object result = null;
        try {
            MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
            Method method = methodSignature.getMethod();
            String[] paramNames = methodSignature.getParameterNames();
            Object[] args = proceedingJoinPoint.getArgs();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return result;
    }
}
