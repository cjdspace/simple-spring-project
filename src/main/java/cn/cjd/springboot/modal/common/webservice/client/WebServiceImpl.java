package cn.cjd.springboot.modal.common.webservice.client;

import cn.cjd.springboot.modal.common.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class WebServiceImpl extends AbstractBaseWebService<String> {

    @Override
    public void doWebService(String url, Map<String, String> headerMap, String params) {
        super.doWebService(url, headerMap, params);
        super.t = HttpUtil.httpPostXml(url, params, headerMap);
        log.info("result: {}", getT());
        doSubmit();
    }

    @Override
    public void doSubmit() {
        super.doSubmit();
    }
}
