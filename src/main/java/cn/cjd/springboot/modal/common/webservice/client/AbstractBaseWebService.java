package cn.cjd.springboot.modal.common.webservice.client;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Getter
@Slf4j
public abstract class AbstractBaseWebService<T> implements BaseService<T> {

    protected T t;

    @Override
    public void doWebService(String url, Map<String, String> headerMap, String params) {
        log.info("getWebService");
    }

    @Override
    public void doSubmit() {
        log.info("doSubmit, T:{}", JSON.toJSONString(t));
    }
}
