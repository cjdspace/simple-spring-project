package cn.cjd.springboot.modal.common.webservice.client;

import java.util.Map;

public interface BaseService<T> {

    void doWebService(String url, Map<String, String> headerMap, String params);

    void doSubmit();
}
