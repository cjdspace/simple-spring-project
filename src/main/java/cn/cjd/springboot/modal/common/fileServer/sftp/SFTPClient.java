package cn.cjd.springboot.modal.common.fileServer.sftp;

import cn.cjd.springboot.modal.common.utils.simpleUtils.StringUtils;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.Properties;

@SuppressWarnings("unused")
@Slf4j
@Component
public class SFTPClient {

    private final SFTPConfig sftpConfig;

    public SFTPClient(SFTPConfig sftpConfig) {
        this.sftpConfig = sftpConfig;
    }

    /**
     * 创建session连接
     */
    private Session createSession() {
        try {
            JSch jsch = new JSch();
            if (!StringUtils.isNullOrEmpty(sftpConfig.getPrivateKey()) && !StringUtils.isNullOrEmpty(sftpConfig.getPassphrase())) {
                jsch.addIdentity(sftpConfig.getPrivateKey(), sftpConfig.getPassphrase());
            } else if (!StringUtils.isNullOrEmpty(sftpConfig.getPrivateKey())) {
                jsch.addIdentity(sftpConfig.getPrivateKey());
            }
            Session session = jsch.getSession(sftpConfig.getUser(), sftpConfig.getIp(), sftpConfig.getPort());
            session.setPassword(sftpConfig.getPassword());
            return session;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("sftp create session error");
        }
    }

    /**
     * 创建channel连接
     */
    private ChannelSftp createChannel(Session session) {
        try {
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config); // 为Session对象设置properties
            session.setTimeout(sftpConfig.getTimeout()); // 设置timeout时间
            session.connect();  // 通过Session建立链接
            Channel channel = session.openChannel("sftp");  // 打开SFTP通道
            channel.connect(); // 建立SFTP通道的连接
            return (ChannelSftp) channel;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("sftp create ChannelSftp failed");
        }
    }

    /**
     * 断开sftp链接
     */
    public static void closeConnection(Channel channel, Session session) {
        try {
            if (session != null) {
                session.disconnect(); //关闭session链接
            }
            if (channel != null) {
                channel.disconnect(); //断开连接
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传文件
     */
    public boolean uploadFile(String filePath) {
        File file = new File(filePath);
        return uploadFile(file);
    }

    /**
     * 上传文件
     */
    public boolean uploadFile(MultipartFile multipartFile) {
        if (Objects.isNull(multipartFile) || multipartFile.isEmpty()) {
            log.error("上传的文件不存在");
            return Boolean.FALSE;
        }
        boolean result;
        try(InputStream inputStream = multipartFile.getInputStream()) {
            result = uploadFile(inputStream, multipartFile.getOriginalFilename());
        } catch (Exception e) {
            result = Boolean.FALSE;
        }
        return result;
    }

    /**
     * 上传文件
     */
    public boolean uploadFile(File file) {
        if (Objects.isNull(file) || !file.exists()) {
            log.error("上传的文件不存在");
            return Boolean.FALSE;
        }
        boolean result;
        try(InputStream inputStream = new FileInputStream(file)) {
            result = uploadFile(inputStream, file.getName());
        } catch (Exception e) {
            result = Boolean.FALSE;
        }
        return result;
    }

    /**
     * 上传文件
     */
    private boolean uploadFile(InputStream inputStream, String fileName) {
        if (Objects.isNull(inputStream)) {
            return Boolean.FALSE;
        }
        boolean result = Boolean.TRUE;
        Session session = null;
        ChannelSftp channelSftp = null;
        OutputStream outputStream = null;
        try {
            session = createSession();
            channelSftp = createChannel(session);
            outputStream = channelSftp.put(sftpConfig.getDst() + "/" + fileName);
            byte[] b = new byte[1024];
            int n;
            while ((n = inputStream.read(b)) != -1) {
                outputStream.write(b, 0, n);
            }
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
            result = Boolean.FALSE;
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            closeConnection(channelSftp, session);
        }
        return result;
    }
}
