package cn.cjd.springboot.modal.common.webservice.server.vo;

import cn.cjd.springboot.modal.common.webservice.server.WebServiceConfig;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "currency", namespace = WebServiceConfig.NAMESPACE_URI)
@XmlAccessorType(XmlAccessType.FIELD)
public enum Currency {

    GBP("GBP"),
    EUR("EUR"),
    PLN("PLN");

    private final String code;

    Currency(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
