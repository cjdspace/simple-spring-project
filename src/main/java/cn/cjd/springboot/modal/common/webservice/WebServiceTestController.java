package cn.cjd.springboot.modal.common.webservice;

import cn.cjd.springboot.modal.common.webservice.client.BaseService;
import cn.cjd.springboot.modal.common.webservice.client.WebServiceImpl;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/webservice")
public class WebServiceTestController {

    private static final String URL = "http://wap.wzhealth.com/feyservice/interface.asmx";

    private static final BaseService<String> webService = new WebServiceImpl();

    @SuppressWarnings("unused")
    @GetMapping("/post")
    public void PostWebService() {
        //组装iPut
        Map<String, String> inputMap = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        inputMap.put("KSSJ", sdf.format(DateUtils.addDays(new Date(), -2)));
        inputMap.put("JZSJ", sdf.format(DateUtils.addDays(new Date(), 1)));
        String inputStr = JSONObject.toJSONString(inputMap);

        //组装postData
//        String postData = getPostData(inputStr, "GetJCInfo");
        String postData = getPostData("{}", "GetJCItems");

        webService.doWebService(URL, null, postData);
    }

    public static String getPostData(String input,String action){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                "  <soap12:Body>\n" +
                "    <DoAction xmlns=\"http://tempuri.org/\">\n" +
                "      <ls_class>NPYHGYJK</ls_class>\n" +
                "      <ls_action>"+action+"</ls_action>\n" +
                "      <ls_input>"+input+"</ls_input>\n" +
                "      <ls_code>"+getIsCode(input)+"</ls_code>\n" +
                "      <ls_output></ls_output>\n" +
                "    </DoAction>\n" +
                "  </soap12:Body>\n" +
                "</soap12:Envelope>";
    }

    public static void main(String[] args) {
        Map<String, String> inputMap = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        inputMap.put("KSSJ", sdf.format(DateUtils.addDays(new Date(), -2)));
        inputMap.put("JZSJ", sdf.format(DateUtils.addDays(new Date(), 1)));
        String inputStr = JSONObject.toJSONString(inputMap);
        System.out.println(getPostData(inputStr, "GetJCInfo"));
    }

    private static final String company = "yihao";

    public static String getIsCode(String input){
        long time = System.currentTimeMillis()/1000;
        String token = "87137646";
        String message = company+ token + time +input;
        String sign =  getMD5Str(message);
        return company+"_"+time+"|"+sign;
    }

    private static String getMD5Str(String str) {
        MessageDigest messageDigest = null;

        try {
            messageDigest = MessageDigest.getInstance("MD5");

            messageDigest.reset();

            messageDigest.update(str.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        }

        byte[] byteArray = messageDigest.digest();

        StringBuilder md5StrBuff = new StringBuilder();

        for (byte b : byteArray) {
            if (Integer.toHexString(0xFF & b).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & b));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & b));
        }

        return md5StrBuff.toString();
    }
}
