package cn.cjd.springboot.modal.common.restTemplate;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

@SuppressWarnings({"WeakerAccess", "unused"})
@Component
public class RestTemplateUtils {

    private static final Logger logger = LoggerFactory.getLogger(RestTemplateUtils.class);

    private final RestTemplate restTemplate;

    @Autowired
    public RestTemplateUtils(@Qualifier("priRestTemplate") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * http 请求处理
     */
    public Object restTemplate(String uri, String method, Object content) {
        return restTemplate(uri, method, content, Object.class);
    }

    /**
     * http 请求处理
     */
    public <T> T restTemplate(String uri, String method, Object content, Class<T> tClass) {
        logger.info("uri: " + uri);
        logger.info("method: " + method);
        logger.info("data: " + JSON.toJSONString(content));
        if (Methods.POST.getCode().equalsIgnoreCase(method)) {
            return this.post(uri, content, tClass);
        } else if (Methods.GET.getCode().equalsIgnoreCase(method)) {
            return this.get(uri, tClass);
        } else {
            return null;
        }
    }

    private HttpHeaders initHttpHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.toString());
        return headers;
    }

    private <T> T post(String uri, Object content, Class<T> tClass) {
        HttpEntity<Object> requestEntity = new HttpEntity<>(content, initHttpHeaders());
        ResponseEntity<T> responseEntity = restTemplate.postForEntity(uri, requestEntity, tClass);
        if (responseEntity.getStatusCode() != HttpStatus.OK)
            throw new RestClientException("http post Exception");
        return responseEntity.getBody();
    }

    private <T> T get(String uri, Class<T> tClass) {
        ResponseEntity<T> responseEntity =  restTemplate.getForEntity(uri, tClass);
        if (responseEntity.getStatusCode() != HttpStatus.OK)
            throw new RestClientException("http post Exception");
        return responseEntity.getBody();
    }
}
