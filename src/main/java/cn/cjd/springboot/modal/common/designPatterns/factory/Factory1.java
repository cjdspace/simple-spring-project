package cn.cjd.springboot.modal.common.designPatterns.factory;

/**
 * 简单工厂模式
 */
@SuppressWarnings("unused")
public class Factory1 {
    public static void main(String[] args) {
        PhoneFactory phoneFactory = new PhoneFactory();
        Android android = (Android) phoneFactory.makePhone("android");
        Apple apple = (Apple) phoneFactory.makePhone("apple");

    }
}

class PhoneFactory {
     Phone makePhone(String phoneType){
        if(phoneType.equalsIgnoreCase("android")){
            return new Android();
        }
        else if(phoneType.equalsIgnoreCase("apple")) {
            return new Apple();
        }
        return null;
    }
}

@SuppressWarnings("unused")
interface Phone {
    void make();
}

class Android implements Phone {

    Android() {
        this.make();
    }

    @Override
    public void make() {
        System.out.println("make android phone");
    }
}

class Apple implements Phone {
    Apple() {
        this.make();
    }

    @Override
    public void make() {
        System.out.println("make apple phone");
    }
}
