package cn.cjd.springboot.modal.common.designPatterns.observer;

public interface DisplayElement {
    void display();
}
