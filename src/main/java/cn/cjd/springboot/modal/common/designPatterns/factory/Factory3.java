package cn.cjd.springboot.modal.common.designPatterns.factory;

/**
 * 抽象工厂模式
 */
@SuppressWarnings("unused")
public class Factory3 {
    public static void main(String[] arg) {
        AbstractFactory3 miFactory = new XiaoMiFactory3();
        AbstractFactory3 appleFactory = new AppleFactory3();
        Phone mi = miFactory.makePhone();            // make xiaomi phone!
        PC miPc = miFactory.makePC();                // make xiaomi PC!
        Phone iphone = appleFactory.makePhone();     // make iphone!
        PC mac = appleFactory.makePC();              // make MAC!
    }
}


interface AbstractFactory3 {
    Phone makePhone();
    PC makePC();
}

class XiaoMiFactory3 implements AbstractFactory3{
    @Override
    public Phone makePhone() {
        return new MiPhone();
    }
    @Override
    public PC makePC() {
        return new MiPC();
    }
}

class AppleFactory3 implements AbstractFactory3 {
    @Override
    public Phone makePhone() {
        return new IPhone();
    }
    @Override
    public PC makePC() {
        return new MAC();
    }
}

@SuppressWarnings("unused")
interface PC {
    void make();
}

class MiPC implements PC {
    MiPC() {
        this.make();
    }
    @Override
    public void make() {
        // TODO Auto-generated method stub
        System.out.println("make xiaomi PC!");
    }
}

class MAC implements PC {
    MAC() {
        this.make();
    }
    @Override
    public void make() {
        // TODO Auto-generated method stub
        System.out.println("make MAC!");
    }
}