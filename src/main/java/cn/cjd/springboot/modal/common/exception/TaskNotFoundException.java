package cn.cjd.springboot.modal.common.exception;

/**
 * @author ponta
 * @date 2018/8/21
 * @email supreme@ponta.io
 */
public class TaskNotFoundException extends Exception{

    public TaskNotFoundException(String message) {
        super(message);
    }
}
