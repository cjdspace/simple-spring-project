package cn.cjd.springboot.modal.common.webservice.server.vo;

import cn.cjd.springboot.modal.common.webservice.server.WebServiceConfig;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getCountryRequest", namespace = WebServiceConfig.NAMESPACE_URI)
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCountryRequest {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
