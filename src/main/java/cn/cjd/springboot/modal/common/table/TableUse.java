package cn.cjd.springboot.modal.common.table;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

public class TableUse {

    public static void main(String[] args) {
        Table<String, String, Object> table = initTable();
    }

    private static Table<String, String, Object> initTable(){
        Table<String, String, Object> table = HashBasedTable.create();
        table.put("row1", "column1", "value1");
        table.put("row2", "column2", "value2");
        table.put("row3", "column3", "value3");
        table.put("row4", "column4", "value4");
        table.put("row5", "column5", "value5");
        return table;
    }
}
