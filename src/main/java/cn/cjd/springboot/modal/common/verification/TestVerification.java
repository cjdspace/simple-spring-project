package cn.cjd.springboot.modal.common.verification;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Random;

@RestController
@RequestMapping("/VerifyImageUtil")
public class TestVerification {
    /**
     * @param @return 参数说明
     * @return BaseRestResult 返回类型
     * @Description: 生成滑块拼图验证码
     */
    @RequestMapping(value = "/getImageVerifyCode", method = RequestMethod.GET)
    public Object getImageCode(HttpServletResponse response, ModelMap modelMap) throws Exception {
//        //读取图库目录
//        File imgCatalog = new File(getClass().getResource("/Users/chengjiade/Desktop/simple-spring-project/src/main/java/cn/cjd/springboot/modal/verification/imagePath").getPath());
//        File[] files = imgCatalog.listFiles();
//
//        int randNum = new Random().nextInt(files.length);
//        File targetFile = files[randNum];
//        File tempImgFile = new File(getClass().getResource("/Users/chengjiade/Desktop/simple-spring-project/src/main/java/cn/cjd/springboot/modal/verification/templatePath/template.png").getPath());
//
//        //根据模板裁剪图片
//        VerifyImageUtil.pictureTemplatesCut(tempImgFile,targetFile,modelMap);
//        modelMap.remove("xWidth");
//        response.addHeader("UUID", UUID.randomUUID().toString());
        return modelMap;
    }

    public static void main(String[] args) throws Exception {
        //读取图库目录
        File imgCatalog = new File("/Users/chengjiade/Desktop/simple-spring-project/src/main/java/cn/cjd/springboot/modal/verification/imagePath");
        File[] files = imgCatalog.listFiles();

        int randNum = new Random().nextInt(files.length);
        File targetFile = files[randNum];
        File tempImgFile = new File("/Users/chengjiade/Desktop/simple-spring-project/src/main/java/cn/cjd/springboot/modal/verification/templatePath/template.png");

        //根据模板裁剪图片
        VerifyImageUtil.pictureTemplatesCut(tempImgFile,targetFile, new ModelMap());
    }
}
