package cn.cjd.springboot.modal.common.designPatterns.observer;

/**
 * 观察者
 */
public interface Observer {
    void update();
}
