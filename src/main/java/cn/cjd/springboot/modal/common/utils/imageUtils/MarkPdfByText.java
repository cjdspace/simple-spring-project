package cn.cjd.springboot.modal.common.utils.imageUtils;

import com.lowagie.text.Element;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.*;

import javax.swing.*;
import java.awt.*;
import java.io.FileOutputStream;

/**
 * 给pdf加水印
 */
public class MarkPdfByText {

    private static final Integer interval = 10;

    public static void main(String[] args) throws Exception {
        String inputFile = "/Users/chengjiade/Downloads/0007t10lmqimvql645a3tde02o.pdf";
        String outputFile = "/Users/chengjiade/Downloads/new.pdf";
        String content = "root用户";
        waterMark(inputFile, outputFile, content);
    }

    public static void waterMark(String inputFile,String outputFile, String waterMarkName) throws Exception {
        PdfReader reader = null;
        PdfStamper stamper = null;
        try {
            reader = new PdfReader(inputFile);
            stamper = new PdfStamper(reader, new FileOutputStream(outputFile));
            BaseFont base = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",   BaseFont.EMBEDDED);
            PdfGState gs = new PdfGState();
            // 设置透明度
            gs.setFillOpacity(0.5f);
            gs.setStrokeOpacity(0.5f);
            int total = reader.getNumberOfPages() + 1;

            JLabel label = new JLabel();
            FontMetrics metrics;
            label.setText(waterMarkName);
            metrics = label.getFontMetrics(label.getFont());
            int textH = metrics.getHeight();
            int textW = metrics.stringWidth(label.getText());

            PdfContentByte under;
            Rectangle pageRect;
            for (int i = 1; i < total; i++) {
                pageRect = reader.getPageSizeWithRotation(i);
                under = stamper.getOverContent(i);
                under.saveState();
                under.setGState(gs);
                under.beginText();
                under.setFontAndSize(base, 16);
                // 水印文字成30度角倾斜
                for (int height = interval + textH; height < pageRect.getHeight(); height = height + textH * 15) {
                    for (int width = interval + textW; width < pageRect.getWidth() + textW; width = width + textW * 4) {
                        under.showTextAligned(Element.ALIGN_LEFT , waterMarkName, width - textW, height - textH, 30);
                    }
                }
                under.endText();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != stamper) {
                stamper.close();
            }
            if (null != reader) {
                reader.close();
            }
        }
    }
}
