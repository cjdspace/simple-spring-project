package cn.cjd.springboot.modal.common.fileServer.sftp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping(value = "/sftp")
public class SftpController {

    private final SFTPClient sftpClient;

    public SftpController(SFTPClient sftpClient) {
        this.sftpClient = sftpClient;
    }

    @PostMapping(value = "/upLoadFile", consumes = "multipart/form-data")
    public void uploadFile(MultipartFile file) {
        boolean result = sftpClient.uploadFile(file);
        if (!result) {
            System.out.println("上传文件失败");
        }
    }


    @Resource
    private SFTPConfig sftpConfig;

    @GetMapping(value = "/show/properties")
    public void showProperties() {
        log.error("IP: {}", sftpConfig.getIp());
        System.out.println("IP: " + sftpConfig.getIp());
    }
}
