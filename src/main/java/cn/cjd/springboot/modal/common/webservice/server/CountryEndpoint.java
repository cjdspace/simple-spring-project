package cn.cjd.springboot.modal.common.webservice.server;

import cn.cjd.springboot.modal.common.webservice.server.vo.Country;
import cn.cjd.springboot.modal.common.webservice.server.vo.GetCountryRequest;
import cn.cjd.springboot.modal.common.webservice.server.vo.GetCountryResponse;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Slf4j
@Endpoint
public class CountryEndpoint {

    @PayloadRoot(namespace = WebServiceConfig.NAMESPACE_URI, localPart = "getCountryRequest")
    @ResponsePayload
    public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
        log.info("request: {}", JSON.toJSONString(request));
        GetCountryResponse response = new GetCountryResponse();
        Country country = new Country();
        country.setName(request.getName());
        response.setCountry(country);
        return response;
    }
}
