package cn.cjd.springboot.modal.common.restTemplate.httpuse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

/**
 * project  dsmp
 * description: module
 *
 * @author chengjiade
 * date:    2021/12/13 2:54 下午
 * @version v1.0
 */
@SuppressWarnings("unused")
@Slf4j
public class HttpUtil {

    private final HttpEntity<JSONObject> requestEntity;
    protected final HttpHeaders headers = new HttpHeaders();

    public HttpUtil(Map<String, String> headersMap) {
        headers.setContentType(MediaType.parseMediaType("application/json;v=1.0"));
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        if (!CollectionUtils.isEmpty(headersMap)) {
            headersMap.forEach(this.headers::set);
        }
        requestEntity = new HttpEntity<>(null, this.headers);
    }

    public File httpGetFile(String url, Map<String, Object> params) {
        try{
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            RestTemplate restTemplate = new RestTemplate(RestTemplateConfig.generateHttpRequestFactory());
            if (!CollectionUtils.isEmpty(params)) {
                params.forEach(builder::queryParam);
            }
            String paramsUrl = builder.build().encode().toString();
            URI uri = new URI(paramsUrl);

            log.info("执行checkmarx的http请求：url:{}, method: GET, param: {}", url, JSON.toJSONString(params));

            RequestCallback requestCallback = request -> {
                request.getHeaders().set("Authorization", headers.getFirst("Authorization"));
                request.getHeaders().setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));
            };

            return restTemplate.execute(uri, HttpMethod.GET, requestCallback, clientHttpResponse -> {
                File ret = File.createTempFile("checkmarx", ".xml");
                StreamUtils.copy(clientHttpResponse.getBody(), new FileOutputStream(ret));
                return ret;
            });
        } catch (Exception e) {
            log.error(">>> API {} GET 请求异常，原因 {}", url, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public <T> T httpGet(String url, MultiValueMap<String, Object> params, Class<T> tClass) {
        try{
            RestTemplate restTemplate = new RestTemplate(RestTemplateConfig.generateHttpRequestFactory());
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            if (!CollectionUtils.isEmpty(params)) {
                params.forEach(builder::queryParam);
            }
            String paramsUrl = builder.build().encode().toString();
            URI uri = new URI(paramsUrl);
            String requestId = UUID.randomUUID().toString().replaceAll("-", "");
            log.info("requestId:{}, 执行checkmarx的http请求：url:{}, method: GET, param: {}", requestId, url, JSON.toJSONString(params));
            T body = restTemplate.exchange(uri, HttpMethod.GET, this.requestEntity, tClass).getBody();
            log.info("requestId:{}, 执行checkmarx的http请求：url:{}, method: GET, response: {}", requestId, url, JSON.toJSONString(body));
            return body;
        } catch (Exception e) {
            log.error(">>> API {} GET 请求异常，原因 {}", url, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public <T> T httpPost(String url, JSONObject params, Class<T> tClass) {
        try {
            RestTemplate restTemplate = new RestTemplate(RestTemplateConfig.generateHttpRequestFactory());
            HttpEntity<JSONObject> httpEntity = new HttpEntity<>(params, this.headers);
            String requestId = UUID.randomUUID().toString().replaceAll("-", "");
            log.info("requestId:{}, 执行checkmarx的http请求：url:{}, method: POST, param: {}", requestId, url, JSON.toJSONString(params));
            T body = restTemplate.postForObject(url, httpEntity, tClass);
            log.info("requestId:{}, 执行checkmarx的http请求：url:{}, method: POST, response: {}", requestId, url, JSON.toJSONString(body));
            return body;
        } catch (Exception e) {
            log.info(">>> API {} POST 请求异常，原因 {}", url, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public <T> T httpPostForm(String url, MultiValueMap<String, Object> params, Class<T> tClass) {
        try {
            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(params, this.headers);
            RestTemplate restTemplate = new RestTemplate(RestTemplateConfig.generateHttpRequestFactory());
            String requestId = UUID.randomUUID().toString().replaceAll("-", "");
            log.info("requestId:{},执行checkmarx的http请求：url:{}, method: POSTForm", requestId, url);
            T body = restTemplate.postForObject(url, httpEntity, tClass);
            log.info("requestId:{}, 执行checkmarx的http请求：url:{}, method: POSTForm, response: {}", requestId, url, JSON.toJSONString(body));
            return body;
        } catch (Exception e) {
            log.info(">>> API {} POST 请求异常，原因 {}", url, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public <T> T httpPut(String url, JSONObject params, Class<T> tClass) {
        try {
            RestTemplate restTemplate = new RestTemplate(RestTemplateConfig.generateHttpRequestFactory());
            HttpEntity<JSONObject> httpEntity = new HttpEntity<>(params, this.headers);
            String requestId = UUID.randomUUID().toString().replaceAll("-", "");
            log.info("requestId:{},执行checkmarx的http请求：url:{}, method: PUT, param: {}", requestId, url, JSON.toJSONString(params));
            T body = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, tClass).getBody();
            log.info("requestId:{},执行checkmarx的http请求：url:{}, method: PUT, response: {}", requestId, url, JSON.toJSONString(body));
            return body;
        } catch (Exception e) {
            log.info(">>> API {} PUT 请求异常，原因 {}", url, e.getMessage());
            throw new RuntimeException(e);
        }
    }
}

