package cn.cjd.springboot.modal.common.designPatterns.singleton;


import java.util.Objects;

// 饿汉模式
class Demo {
    private static Demo demo = new Demo();
    private Demo() {}
    public static Demo getInstance(){
        return demo;
    }
}

// 懒汉模式
class Demo1 {
    private static Demo1 demo1;
    private Demo1() {}
    public static Demo1 getInstance() {
        if (Objects.isNull(demo1)){
            demo1 = new Demo1();
        }
        return demo1;
    }
}


// 双重校验锁模式
class Demo2 {
    private static Demo2 demo2;
    private Demo2() {}
    public static Demo2 getInstance() {
        if (Objects.isNull(demo2)) {
            synchronized (Demo2.class) {
                if (Objects.isNull(demo2)) {
                    demo2 = new Demo2();
                }
            }
        }
        return demo2;
    }
}

// 静态内部类
class Demo3 {
    private static class SingletonHolder{
        private static final Demo3 demo3 = new Demo3();
    }
    private Demo3(){}
    public static Demo3 getInstance() {
        return SingletonHolder.demo3;
    }

}

