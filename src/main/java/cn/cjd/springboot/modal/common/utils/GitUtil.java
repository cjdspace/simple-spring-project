package cn.cjd.springboot.modal.common.utils;

import com.alibaba.fastjson.JSON;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.*;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.StringUtils;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * project  dsmp
 * description: module
 *
 * @author chengjiade
 * date:    2021/12/22 10:21 上午
 * @version v1.0
 */
public class GitUtil {

    private final String uri;

    private final String branch;

    private final String savePath;

    private final String privateKey;

    public GitUtil(String uri, String branch, String savePath, String privateKey) {
        this.uri = uri;
        this.branch = branch;
        this.savePath = savePath;
        this.privateKey = privateKey;
    }

    private String privateFilePath;

    /**
     * 检测git联通
     */
    public Boolean testConnect() {
        try {
            LsRemoteCommand lsRemoteCommand = Git.lsRemoteRepository();
            lsRemoteCommand.setRemote(uri);
            if (!StringUtils.isEmptyOrNull(privateKey)) {
                File file = createRsaTempFile(privateKey);
                privateFilePath = file.getAbsolutePath();
                lsRemoteCommand.setTransportConfigCallback(sshAuth());
            }
            lsRemoteCommand.setRemote(uri).call();
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }


    /**
     * 无认证的拉取
     * 账号密码形式的拉取
     * 用户令牌的形式拉取
     * 私钥形式的拉取
     * 其中账号密码形式/用户令牌形式，将信息直接填写至uri中
     */
    public void cloneProject() throws IOException, InterruptedException, GitAPIException {

        String projectName = uri.substring(uri.lastIndexOf("/") + 1).replace(".git", "");
        String savePath = this.savePath.endsWith("/") ? this.savePath + projectName : this.savePath + "/" + projectName;

        CmdUtil.exec("rm -rf " + savePath + " && mkdir -p " + savePath);

        CloneCommand cloneCommand = Git.cloneRepository();
        cloneCommand.setURI(uri)
                .setBranch(branch)
                .setDirectory(new File(savePath));
        if (!StringUtils.isEmptyOrNull(privateKey)) {
            File file = createRsaTempFile(privateKey);
            privateFilePath = file.getAbsolutePath();
            cloneCommand.setTransportConfigCallback(sshAuth());
        }

        cloneCommand.call();
    }

    /**
     * 列出远端分支
     */
    public List<String> lsBranch() throws IOException, GitAPIException {
        LsRemoteCommand lsRemoteCommand = Git.lsRemoteRepository();
        lsRemoteCommand.setRemote(uri);
        if (!StringUtils.isEmptyOrNull(privateKey)) {
            File file = createRsaTempFile(privateKey);
            privateFilePath = file.getAbsolutePath();
            lsRemoteCommand.setTransportConfigCallback(sshAuth());
        }
        Collection<Ref> refList = lsRemoteCommand.setRemote(uri).call();
        if (CollectionUtils.isEmpty(refList)) {
            return new ArrayList<>();
        }
        return refList.stream().map(Ref::getName).filter(name -> !"HEAD".equals(name)).filter(name -> name.contains("refs/heads/")).collect(Collectors.toList());
    }

    private TransportConfigCallback sshAuth() {
        return transport -> {
            SshTransport sshTransport = (SshTransport) transport;
            sshTransport.setSshSessionFactory(new JschConfigSessionFactory() {
                @Override
                protected void configure(OpenSshConfig.Host host, Session session) {
                    session.setConfig("StrictHostKeyChecking", "no");
                }

                @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
                @Override
                protected JSch createDefaultJSch(FS fs) throws JSchException {
                    JSch jSch = super.createDefaultJSch(fs);
                    jSch.addIdentity(privateFilePath);
                    return jSch;
                }
            });
        };
    }

    public static File createRsaTempFile(String privateKey) throws IOException {
        File file = File.createTempFile("checkmarx", "_rsa");
        try (FileOutputStream fileOutputStream = new FileOutputStream(file);
             OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
             BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter)) {

            String[] words = privateKey.split("\n");

            for (String word : words) {
                bufferedWriter.write(word);
                bufferedWriter.newLine();
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        System.out.println("filePath: " + file.getAbsolutePath());
        return file;
    }

    public static void main(String[] args) throws GitAPIException, IOException, InterruptedException {

//        GitUtil gitUtil = new GitUtil("https://15274917013:fb1973c49c659d7e135af22f176ad442@gitee.com/cjdspace/moresec-archtype.git", "refs/heads/master",
//                "/Users/chengjiade/Downloads/git",
//                null);

        GitUtil gitUtil = new GitUtil("git@gitee.com:cjdspace/moresec-archtype.git", "refs/heads/master",
                "/Users/chengjiade/Downloads/git",
                privateKey());

        System.out.println("测试联通");
        Boolean aBoolean = gitUtil.testConnect();
        System.out.println(aBoolean);

        System.out.println("列举分支");
        List<String> strings = gitUtil.lsBranch();
        System.out.println("strings: " + JSON.toJSONString(strings));

        System.out.println("克隆");
        gitUtil.cloneProject();
    }

    @SuppressWarnings("unused")
    private static String privateKey() {
        return "-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIIG5AIBAAKCAYEAraRz8ak44vk2Jq5fCziQIhD3OUQP4JVOkDYmDZJhjigIVtwI\n" +
                "o8vFhjl7bcLZGzVnexHp7XJbsTdaZvs+zUL3eH/4QlXPwwAQmVtj9RlBJf7aatx0\n" +
                "0uvly+NWX4NlCya05Ws3ofazG10xqxC3NlMKzr1v2TaSe3GSr9ZGKaXENQAxM2D+\n" +
                "y7DjuatqMYQh5pPd/feK6IHbLMLS8iu8hNLAflgBR7eS/lyAa/yDmPk90vE3cCV6\n" +
                "+JVd8PraZBkAoPf64TLMZckH1hYd+XtTwI+U0Z9CNkDR5aZhIFOhbqolAsL2/kkq\n" +
                "iBdtiCiZfYPGHwft0hHJrKGgbzAnEgyONnRa361isTsjj/SUIT3NNF6OGh8uArBe\n" +
                "j9SL5QxuLVEBfdSjfvz2wHkPQIOFssD745ei37suWV32JHwNGOQXROaHVOLE35rE\n" +
                "JFUY6H8xMb9gLNgrFtta1FyBrGFv6jhZ+NQ8o8da4VITGg4MsEtOQf/FkDnLKnVA\n" +
                "NZoKoC79ImbePTUvAgMBAAECggGAPrbkwWVzDFch/bqAzVayh/tEr0Qrz9tdMqy7\n" +
                "9a7m2ScQS/7QiAgy4RWlxLDZJQCWzVZc/MuO3mS1m/gmaJBFPRpLGfpQPYeqdxkQ\n" +
                "8xcZ9pUjYfEZqIxWxNro+TpFpk9mim28/BxT+BAivjwdT9uSTbURzovtP/R3HFbx\n" +
                "pF0gjlvxaTMhE4Rh209T3WpxrAu/49giLXOpK3N6KMkvH3HW4Er8u2BMEN5r65Sa\n" +
                "Gwf6ElPYyAJhdaLM+RpBAmIdXgXx2C65HLQ+XfZBEumSqyHmoU7+9hEGZvUznhsY\n" +
                "s108ibdcO47yoxd5wJ0wVt5DDjkDNeJ3f6pZbVx0QC0EUwTwUmHbz6dFBA7757JQ\n" +
                "oJlS2M3HwL3l7k1KA5iQIiFsSgFoP4OXqAao3ugxZRbwVQ8J0oLlWB1TOD9kvf/X\n" +
                "qFQcgK1+grMPZcN3MJId/+FwR9jvIjWIItiYRBCT9YojTxGPgLgr2qe5SuU3lI5C\n" +
                "C0mtj0ZbG6oFwPKcApScmHlniUYBAoHBANw6bXhzT8qTWSgflf4nAYxCuJSwS3o/\n" +
                "egXvXFj+X2FfrTBa4GTG9+dT6CyNCEoFu5o3lxRL0Y3DbjbcGldxFmm2XbhrG/jR\n" +
                "xhvkvS5MR3JZ0VGgJVT4XfA27hsZ2PBrqgd2wznqZikVfdy2zsUjtuRVMYw8GcrW\n" +
                "yRGICk1fxzg0+DMVURpa8Ei22nc+goqqtS0Xs9FlU2IZn+vMSwpC+LQgaKR6qaKi\n" +
                "bm6iavHNGg0T0uPyBtfwsd9jBdXwsnz3gQKBwQDJ2OJRiRSUQPT0i2uC+yBP4Mj/\n" +
                "a6lEUcjviI67fjf7X3XQJWGKwFikXjt4pldA94nQIufAT3rpwPpj/XapPyYDFP9f\n" +
                "9T+6vXyYabyE5J6VP4xWrBZm+T9yMxl4DtAAk1MjVsuuM/mE9NoSu2c9prOH1tCp\n" +
                "43aVA+7PZPzktcnQ0eNtaEpBjxN9kfl6dfZM39a93kavjKixBHnFuhI//3I+m6CN\n" +
                "DYhewXy1qs583lKOazo0rTc2w/a7NZOGoG/yBK8CgcEAhRII8685l35RcR0k1N1c\n" +
                "4hOu1Klx8fxJDy0C8ANEPuJBE+bVkA9p3C2nLMRLLe8o3Jaag3kMtkxQXHXCtPNw\n" +
                "R5AaMUUHMeIPgutEJKWSxlXfVSMfa6hnmq6WUWnc2+kyZDAu5t/ruwdxddxANH7Q\n" +
                "FEgmGEZ4j0gZVq6NCxPEnQHkt3k5+GLarhVPDJ6I4qEjuIuJ7Az9xVkWeLSI4zp0\n" +
                "bOLoV/qGZe8gK1mYpWL5Ss2fsHyeBf06fXCoJg26zCgBAoHAW0nD0ax1qjdWkeMc\n" +
                "VLjO/FFlll9SxptWjT7tjs6e8wlRdznwy5gCfxQ/QWm5vKA3q/KdbdSEeoPUurQ/\n" +
                "nnv47RvqA7Qfho18dFId+NyM/GMpTWm3GAtSLov90NaiLCB1UV5H8BmBmChFGIO5\n" +
                "RaDHQYBeDvfZAbwtnY7G8YlmJRNIEItCSjQzVG9YrZH8OvhhXq3QukSbZrPnfw2Q\n" +
                "g3E8ciRgkGq2amSHyoHdsYu7Or40LO7KMfKe/DxW5GRbPNHxAoHBAISlntwONscM\n" +
                "VvChD550z05JB3vvk8wX5P8e2VPM7zpicukAQT6DzCw/AKMi1J0i9sKjzOJROpLk\n" +
                "0na0fJsqMsVWj7SevvlRjfvw6t1YE0lIdDIH2oeHXX3Y7/RU1BA+cPp8DiTpx5L5\n" +
                "bphO2s1ivRWq1bQuPnnIhx/BMxjW/nBXHkNB9yWAlvfkRf+orCxgqBZahruWGJZS\n" +
                "a9Cy+UpOJh0jb8aDxdwpX7aEI0dKBdA030EhlCfbmPEH9T8WlfJ9LQ==\n" +
                "-----END RSA PRIVATE KEY-----";
    }
}


