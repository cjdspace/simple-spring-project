package cn.cjd.springboot.modal.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @ClassName: CmdUtil
 * @Project: ms_server
 * @Description: 系统命令工具类
 * @Author: zhengwei
 * @Date: 2021/11/3 16:13
 **/
@Component
@Slf4j
public class CmdUtil {

    /**
     * 执行系统命令，并返回执行结果
     * @param cmd 命令，如 "ping -c 4 www.baidu.com"
     * @return 返回结果
     */
    public static String exec(String cmd) throws IOException, InterruptedException {
        StringBuilder result = new StringBuilder();
        if (cmd != null && cmd.length() > 0) {
            Runtime runtime = Runtime.getRuntime();
            Process p = runtime.exec(cmd);
            InputStream in = p.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                if (result.length() > 0) {
                    result.append("\n");
                }
                result.append(line);
            }
            p.waitFor();
            in.close();
            reader.close();
            p.destroy();
        }
        return result.toString();
    }
}
