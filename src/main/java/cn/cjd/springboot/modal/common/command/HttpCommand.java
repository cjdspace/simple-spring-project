package cn.cjd.springboot.modal.common.command;

public interface HttpCommand<T> {

    boolean exec(HttpCommandContext<T> commandContext);

    boolean rollBack(HttpCommandContext<T> commandContext);
}
