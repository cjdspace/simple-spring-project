package cn.cjd.springboot.modal.thread.someThread.use2;

public class PrintThread implements Runnable {

    private Number number;
    private ThreadLock threadLock;
    private Integer currentState;
    private Integer nextState;

    public PrintThread(Number number, ThreadLock threadLock, Integer currentState, Integer nextState) {
        this.number = number;
        this.threadLock = threadLock;
        this.currentState = currentState;
        this.nextState = nextState;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (threadLock) {
                if (currentState != threadLock.getState()) {
                    try {
                        threadLock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (number.isEnd()) {
                    System.out.println(Thread.currentThread().getName()+ " break;");
                    threadLock.notifyAll();
                    break;
                }
                for (int i = 0; i < 3; i++) {
                    if (!number.isEnd()){
                        System.out.println(Thread.currentThread().getName() + " " + number.getNum());
                    }
                }
                threadLock.setState(nextState);
                threadLock.notifyAll();
            }
        }
    }
}
