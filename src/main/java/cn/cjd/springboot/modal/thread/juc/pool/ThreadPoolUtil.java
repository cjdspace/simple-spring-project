package cn.cjd.springboot.modal.thread.juc.pool;

import org.nutz.lang.Lang;
import org.springframework.stereotype.Component;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class ThreadPoolUtil {
    private volatile static ExecutorService executorService;

    public static ExecutorService getExecutorService() {
        if (null != executorService) {
            return executorService;
        }
        synchronized (ThreadPoolUtil.class) {
            if (Lang.isEmpty(executorService)) {
                executorService = new ThreadPoolExecutor(4, 8, 5,
                        TimeUnit.SECONDS,
                        new SynchronousQueue<>(),
                        new DefaultThreadFactory(),
                        new ThreadPoolExecutor.CallerRunsPolicy());
            }
        }
        return executorService;
    }

    /**
     * 设置核心线程数为4时，当没有提交任务，则存活的线程数为0，
     * 提交至4个时，才会存活4个核心线程数
     */
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        ExecutorService executorService = ThreadPoolUtil.getExecutorService();
        executorService.submit(()-> System.out.println("121312313"));
        executorService.submit(()-> System.out.println("121312313"));
        executorService.submit(()-> System.out.println("121312313"));
        executorService.submit(()-> System.out.println("121312313"));

        countDownLatch.await();
    }
}


class DefaultThreadFactory implements ThreadFactory {
    //(1)
    private static final AtomicInteger poolNumber = new AtomicInteger(1);
    private final ThreadGroup group;
    //(2)
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    //(3)
    private final String namePrefix;

    DefaultThreadFactory() {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        namePrefix = "sy-workflow-" + poolNumber.getAndIncrement() + "-thread-";
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
        if (t.isDaemon())
            t.setDaemon(false);
        if (t.getPriority() != Thread.NORM_PRIORITY)
            t.setPriority(Thread.NORM_PRIORITY);
        return t;
    }
}

