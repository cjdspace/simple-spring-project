package cn.cjd.springboot.modal.thread.someThread.use2;

import java.util.stream.IntStream;

/**
 * 有 1-90个数，由3个线程依次打印数字，每个线程每次打印3个数字，循环打印
 * 结果如下
 * 线程1 1
 * 线程1 2
 * 线程1 3
 * 线程2 4
 * 线程2 5
 * 线程2 6
 * 线程3 7
 * 线程3 8
 * 线程3 9
 * 。。。
 * 线程3 88
 * 线程3 89
 * 线程3 90
 */
public class Test {

    private static Number number = new Number(1, 90);
    private static ThreadLock threadLock = new ThreadLock(1);

    public static void main(String[] args) {
//        IntStream.range(1, 4).forEach(a -> {
//            System.out.println("a = " + a);
//            int nextState = a + 1;
//            if (a == 3){
//                nextState = 1;
//            }
//            new Thread(new PrintThread(number, threadLock, a, nextState), "线程" + a).start();
//        });


        new Thread(new PrintThread(number, threadLock, 1, 2), "线程1").start();
        new Thread(new PrintThread(number, threadLock, 2, 3), "线程2").start();
        new Thread(new PrintThread(number, threadLock, 3, 1), "线程3").start();
    }
}

class Number {
    private int startNum;
    private int endNum;

    public Number(int startNum, int endNum) {
        this.startNum = startNum;
        this.endNum = endNum;
    }

    public boolean isEnd() {
        if (startNum > endNum) {
            return true;
        }
        return false;
    }
    public int getNum() {
        return startNum++;
    }
}

class ThreadLock {
    private int state;

    public ThreadLock(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}