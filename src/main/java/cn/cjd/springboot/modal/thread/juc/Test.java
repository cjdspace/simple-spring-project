package cn.cjd.springboot.modal.thread.juc;

import sun.misc.Unsafe;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.*;

public class Test {

//    public static void main(String[] args) throws Exception {
//        CountSave save = new CountSave() ;
//        // 从0-100的累加处理操作
//        AddTask task = new AddTask(save,0, 100);
//        ForkJoinPool pool = new ForkJoinPool() ;
//        // 提交任务
//        pool.submit(task);
//        while(!task.isDone()) {	// 当前的任务没有结束
//            System.out.println("活跃线程：" + pool.getActiveThreadCount()
//                    + "、最大的并发线程数：" + pool.getParallelism());
//            // TimeUnit.MILLISECONDS.sleep(100);
//        }
//        if (task.isCompletedNormally()) {	// 分支任务计算完成
//            System.out.println("计算完成了：" + save.getSum());
//        }
//    }

//    public static void main(String[] args) throws Exception {
//        // 当凑够2个线程就进行触发
//        CyclicBarrier cb = new CyclicBarrier(2);
//        for (int x = 0; x < 3; x++) {
//            int sec = x ;
//            new Thread(() -> {
//                System.out.println("【" + Thread.currentThread().getName() + " - 等待开始】");
//                try {
//                    TimeUnit.SECONDS.sleep(sec);
//                    //              cb.await(); // 等待处理
//                    cb.await(6,TimeUnit.SECONDS); // 等待处理,设置超时时间6秒
//                } catch (Exception e) {
//                    System.out.println("【" + Thread.currentThread().getName() + " - 异常】");
//                    e.printStackTrace();
//                }
//                System.out.println("【" + Thread.currentThread().getName() + " - 等待结束】");
//            }, "娱乐者-" + x).start();
//        }
//    }

    public static void main(String[] args) throws NoSuchFieldException {
        AtomicInteger a = new AtomicInteger(123);
    }
}

class CountSave {  // 保存数据处理结果
    private Lock lock = new ReentrantLock() ;
    private int sum = 0 ;  // 保存处理结果
    public void add(int sum) {
        this.lock.lock();
        try {
            this.sum += sum ;
        } finally {  this.lock.unlock();   }
    }
    public int getSum() {
        return this.sum ;
    }
}

class AddTask extends RecursiveAction {
    private int start;
    private int end;
    private CountSave save ;
    // 传入计算的开始和结束的值
    public AddTask(CountSave save,int start, int end) {
        this.save = save ;
        this.start = start;
        this.end = end;
    }
    @Override
    protected void compute() {
        // 开启了分支
        if (end - start < 100) {
            int sum = 0; // 保存求和的计算结果
            for (int x = start; x <= end; x++) {
                sum += x;
            }
            this.save.add(sum); // 保存计算结果
        } else {
            // 中间值
            int middle = (start + end) / 2;
            // 做0 - 50累加
            AddTask leftTask = new AddTask(this.save,start, middle);
            // 做51 - 100累加
            AddTask rightTask = new AddTask(this.save,middle + 1, end);
            // 并行执行的任务
            super.invokeAll(leftTask, rightTask);
        }
    }
}



class Teacher implements Runnable { // 老师也设置一个多线程
    private int studentCount;       // 参与考试的学生数量
    private int submitCount = 0 ;   // 保存交卷的学生个数
    private DelayQueue<Student> students;
    public Teacher(DelayQueue<Student> students, int studentCount) {
        // 保存所有的学生信息
        this.students = students ;
        // 保存学生数量
        this.studentCount = studentCount ;
    }
    @Override
    public void run() {
        System.out.println("********** 同学们开始答题 ************");
        try {
            // 还有未交卷
            while (this.submitCount < this.studentCount) {
                Student stu = this.students.poll() ;
                // 有人出队列了，就表示有人交卷了
                if (stu != null) {
                    stu.exam(); // 交卷处理
                    this.submitCount ++ ;  // 交卷的学生个数加1
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("********** 同学们结束考试 ************");
    }
}
class Student implements Delayed {
    private String name ;
    // 学生交卷时间，使用毫秒单位
    private long submitTime ;
    // 实际的考试时间
    private long workTime ;
    public Student(String name,long workTime,TimeUnit unit) {
        // 保存名字
        this.name = name ;
        // 毫秒是存储单位
        this.workTime = TimeUnit.MILLISECONDS.convert(workTime, unit) ;
        // 交卷时间
        this.submitTime = System.currentTimeMillis() + this.workTime ;
    }
    public void exam() {   // 考试处理
        System.out.println("【"+this.name+"交卷 -｛"+this.submitTime+"｝】交卷时间：" + System.currentTimeMillis() + "、花费时间：" + this.workTime);
    }
    @Override
    public int compareTo(Delayed o) {
        if (Objects.isNull(o)){
            return 1;
        }
        Student os = (Student) o;
        return (int) (this.workTime - os.workTime);
    }

    @Override
    public long getDelay(TimeUnit timeUnit) {
        return timeUnit.convert(this.submitTime - System.currentTimeMillis(), TimeUnit.NANOSECONDS);
    }
}