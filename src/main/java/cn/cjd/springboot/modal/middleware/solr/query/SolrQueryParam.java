package cn.cjd.springboot.modal.middleware.solr.query;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.apache.solr.client.solrj.SolrQuery;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

@Data
public class SolrQueryParam {

    private String q;

    private List<String> fq;

    private List<SolrQuery.SortClause> sortClauseList;

    private Integer start;

    private Integer rows;

    private String f1;

    private String df;

    // 额外的查询条件(格式为：key1=val1&key2=val2)
    private String RawQueryParameters;

    // 查询结果返回形式
    private WT wt = WT.json;

    // 是否结果值为一行
    private Boolean indentOFF;


    public SolrQuery translateToMap() {
        SolrQuery solrQuery = new SolrQuery();
        if (!Objects.isNull(getQ())) {
            solrQuery.add("q", getQ());
        }
        if (!CollectionUtils.isEmpty(getFq())) {
            solrQuery.add("fq", JSON.toJSONString(getFq().toArray()));
        }
        if (!CollectionUtils.isEmpty(getSortClauseList())) {
            getSortClauseList().forEach(solrQuery::addSort);
        }
        if (!Objects.isNull(getStart())) {
            solrQuery.setStart(getStart());
        }
        if (!Objects.isNull(getRows())) {
            solrQuery.setRows(getRows());
        }
        if (!Objects.isNull(getRawQueryParameters())) {
            // 解析操作
        }
        if (!Objects.isNull(getWt())) {
            solrQuery.add("wt", getWt().toString());
        }
        if (!Objects.isNull(getIndentOFF())) {
            solrQuery.add("indent", getIndentOFF().toString());
        }
        return solrQuery;
    }
}
