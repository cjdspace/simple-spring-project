package cn.cjd.springboot.modal.middleware.solr.service;

import cn.cjd.springboot.modal.middleware.solr.query.SolrQueryParam;
import cn.cjd.springboot.modal.middleware.solr.document.BaseSolrEntity;

import java.util.List;

public interface SolrService<EntityImpl extends BaseSolrEntity> {

    void addBean(EntityImpl entity);

    void addBeans(List<EntityImpl> entities);

    EntityImpl getById(String id);

    void delById(String id);

    List<EntityImpl> queryFormSolr(SolrQueryParam solrQueryParam);
}
