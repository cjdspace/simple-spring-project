package cn.cjd.springboot.modal.middleware.MQ.rabbit.simple.queue;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * 如果有两个缴费者绑定同一条队列，则采用轮询的方式发送消息
 **/
@SuppressWarnings("unused")
@Slf4j
@Component
public class SimpleReceive {

//    @RabbitHandler
//    @RabbitListener(queues = SimpleConfig.SIMPLE_QUEUE1_NAME)
//    public void SimpleQueue1Receive_1(byte[] bytes) {
//        String msg = new String(bytes, StandardCharsets.UTF_8);
//        log.info("simple has been consumption, receiveName: {} content: {}", "SimpleQueue1Receive_1", msg);
//    }

    @RabbitHandler
    @RabbitListener(queues = SimpleConfig.SIMPLE_QUEUE1_NAME)
    public void SimpleQueue1Receive_2(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("simple has been consumption, receiveName: {} content: {}", "SimpleQueue1Receive_2", msg);
    }

    @RabbitHandler
    @RabbitListener(queues = SimpleConfig.SIMPLE_QUEUE1_NAME_CHANNEL)
    public void SimpleQueue1ReceiveChannel_1(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("simple has been consumption, receiveName: {} content: {}", "SimpleQueue1Receive_1", msg);
    }

    @RabbitHandler
    @RabbitListener(queues = SimpleConfig.SIMPLE_QUEUE1_NAME_CHANNEL)
    public void SimpleQueue1ReceiveChannel_2(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("simple has been consumption, receiveName: {} content: {}", "SimpleQueue1Receive_2", msg);
    }
}
