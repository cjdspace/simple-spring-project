package cn.cjd.springboot.modal.middleware.MQ.rabbit.delay;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Slf4j
@Component
public class DelaySender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public DelaySender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void delaySend() {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        String message= "delaySend";
        rabbitTemplate.convertAndSend("", DelayConfig.DEAD_QUEUE_NAME, message, message1 -> {
            message1.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            message1.getMessageProperties().setContentType("application/json");
            message1.getMessageProperties().setContentEncoding(StandardCharsets.UTF_8.toString());
            // 设置消息延时时间 60000毫秒
            message1.getMessageProperties().setExpiration("60000");
            return message1;
        });

    }
}
