package cn.cjd.springboot.modal.middleware.MQ.rabbit.persisitence;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class PersistenceConfig {

    public static final String PERSISTENT_EXCHANGE = "persistent_exchange";

    public static final String PERSISTENT_QUEUE_NAME = "persistent_queue";
    public static final String PERSISTENT_QUEUE_KEY = "persistent_queue_key";

    @Bean
    DirectExchange persistentExchange() {
        return new DirectExchange(PERSISTENT_EXCHANGE, true, false);
    }

    @Bean
    Queue persistentQueue() {
        return new Queue(PERSISTENT_QUEUE_NAME, true);
    }

    @Bean
    Binding persistentQueueToExchange(Queue persistentQueue, DirectExchange persistentExchange) {
        return BindingBuilder.bind(persistentQueue).to(persistentExchange).with(PERSISTENT_QUEUE_KEY);
    }
}
