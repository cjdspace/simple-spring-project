package cn.cjd.springboot.modal.middleware.solr.conf;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SolrConfig {

    @Value("${spring.data.solr.core}")
    public static String coreName;

    @Bean("mySolrClient")
    public SolrClient solrClient() {
        return new HttpSolrClient("http://62.234.137.124:8983/solr/new_core");
    }
//
//    @Bean("solrTemplate")
//    public SolrTemplate solrTemplate() {
//        return new SolrTemplate(solrClient());
//    }
}
