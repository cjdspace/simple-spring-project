package cn.cjd.springboot.modal.middleware.solr.ctrl;

import cn.cjd.springboot.modal.middleware.solr.service.DemoSolrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/solr")
public class SolrController {

    private final DemoSolrService demoSolrService;

    @Autowired
    public SolrController(DemoSolrService demoSolrService) {
        this.demoSolrService = demoSolrService;
    }

    @RequestMapping(value = "/addDemoDocument", method = RequestMethod.GET)
    public void addDemoDocument() {
        demoSolrService.addDemoDocument();
    }
}
