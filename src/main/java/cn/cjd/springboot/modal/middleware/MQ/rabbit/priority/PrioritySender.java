package cn.cjd.springboot.modal.middleware.MQ.rabbit.priority;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PrioritySender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public PrioritySender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void prioritySend() {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());

        String message1 = "prioritySendPriority8";
        rabbitTemplate.convertAndSend(PriorityConfig.PRIORITY_EXCHANGE, PriorityConfig.PRIORITY_QUEUE_KEY, message1, message -> {
            message.getMessageProperties().setPriority(8);
            return message;
        });

        String message2 = "prioritySendPriority1";
        rabbitTemplate.convertAndSend(PriorityConfig.PRIORITY_EXCHANGE, PriorityConfig.PRIORITY_QUEUE_KEY, message2, message -> {
            message.getMessageProperties().setPriority(1);
            return message;
        });

        String message3 = "prioritySendPriority9";
        rabbitTemplate.convertAndSend(PriorityConfig.PRIORITY_EXCHANGE, PriorityConfig.PRIORITY_QUEUE_KEY, message3, message -> {
            message.getMessageProperties().setPriority(9);
            return message;
        });
    }
}
