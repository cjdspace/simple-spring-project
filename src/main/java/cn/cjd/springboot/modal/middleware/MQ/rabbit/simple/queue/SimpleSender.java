package cn.cjd.springboot.modal.middleware.MQ.rabbit.simple.queue;

import com.rabbitmq.client.Channel;
import org.apache.commons.compress.utils.CharsetNames;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

@Component
public class SimpleSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public SimpleSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * 当没有使用交换机时，routeKey即为队列名称
     */
    public void simpleSend() {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        for (int i = 0; i < 10 ; i++) {
            String message = "SimpleSend" + i;
            rabbitTemplate.convertAndSend("",SimpleConfig.SIMPLE_QUEUE1_NAME, message);
        }
    }

    public void simpleSendWithChannel() {
        Connection connection = null;
        Channel channel = null;
        try {
            rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
            connection = rabbitTemplate.getConnectionFactory().createConnection();
            channel = connection.createChannel(false);
            for (int i = 0; i < 10 ; i++) {
                String message = "SimpleSend" + i;
                channel.basicPublish("",SimpleConfig.SIMPLE_QUEUE1_NAME_CHANNEL,null, message.getBytes(StandardCharsets.UTF_8));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException | TimeoutException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
}
