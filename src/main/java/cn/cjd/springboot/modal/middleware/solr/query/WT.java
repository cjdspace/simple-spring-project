package cn.cjd.springboot.modal.middleware.solr.query;

public enum WT {
    json,
    xml,
    python,
    ruby,
    php,
    csv;
}
