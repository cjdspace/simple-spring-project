package cn.cjd.springboot.modal.middleware.solr.document;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(solrCoreName = "new_core")
public class DemoDocument extends BaseSolrEntity {

    @Field
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
