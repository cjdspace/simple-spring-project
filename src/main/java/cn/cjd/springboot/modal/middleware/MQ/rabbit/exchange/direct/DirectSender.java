package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.direct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Slf4j
@Component
public class DirectSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public DirectSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void directSend() {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        String message = "DirectSender_" + DirectConfig.DIRECT_QUEUE1_KEY;
        rabbitTemplate.convertAndSend(DirectConfig.DIRECT_EXCHANGE_NAME,DirectConfig.DIRECT_QUEUE1_KEY,message, message1 -> {
            message1.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            message1.getMessageProperties().setContentType("application/json");
            message1.getMessageProperties().setContentEncoding(StandardCharsets.UTF_8.toString());
            return message1;
        });
        message = "DirectSender_" + DirectConfig.DIRECT_QUEUE2_KEY;
        rabbitTemplate.convertAndSend(DirectConfig.DIRECT_EXCHANGE_NAME,DirectConfig.DIRECT_QUEUE2_KEY,message);
    }
}
