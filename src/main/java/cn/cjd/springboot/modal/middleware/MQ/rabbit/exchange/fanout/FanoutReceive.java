package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.fanout;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@SuppressWarnings("unused")
@Slf4j
@Component
public class FanoutReceive {

    /**
     * FANOUT_QUEUE1_NAME 一个消费者
     * FANOUT_QUEUE2_NAME 一个消费者
     * FANOUT_QUEUE3_NAME 两个消费者
     *
     * fanout_exchange接收到消息会分发到3个queue，一个queue一份
     *
     * 由于queue3有两个消费者则会由其中的一个消费者进行消费，两个消费者轮询消费队列消息
     */

    @RabbitHandler
    @RabbitListener(queues = FanoutConfig.FANOUT_QUEUE1_NAME)
    public void FanoutQueue1Receive(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("FanoutQueue1Receive content: {}", msg);
    }

    @RabbitHandler
    @RabbitListener(queues = FanoutConfig.FANOUT_QUEUE2_NAME)
    public void FanoutQueue2Receive(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("FanoutQueue2Receive content: {}", msg);
    }

    @RabbitHandler
    @RabbitListener(queues = FanoutConfig.FANOUT_QUEUE3_NAME)
    public void FanoutQueue3Receive_1(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("FanoutQueue3Receive_1 content: {}", msg);
    }

    @RabbitHandler
    @RabbitListener(queues = FanoutConfig.FANOUT_QUEUE3_NAME)
    public void FanoutQueue3Receive_2(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("FanoutQueue3Receive_2 content: {}", msg);
    }
}
