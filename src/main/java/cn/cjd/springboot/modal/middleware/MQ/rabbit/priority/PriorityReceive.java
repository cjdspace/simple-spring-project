package cn.cjd.springboot.modal.middleware.MQ.rabbit.priority;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Slf4j
@Component
public class PriorityReceive {

    @RabbitHandler
    @RabbitListener(queues = PriorityConfig.PRIORITY_QUEUE_NAME)
    public void priorityQueueReceive(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("priorityQueueReceive content: {}", msg);
    }
}
