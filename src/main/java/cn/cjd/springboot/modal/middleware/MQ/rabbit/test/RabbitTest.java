package cn.cjd.springboot.modal.middleware.MQ.rabbit.test;

import cn.cjd.springboot.modal.middleware.MQ.rabbit.confirm.ConfirmSender;
import cn.cjd.springboot.modal.middleware.MQ.rabbit.delay.DelaySender;
import cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.direct.DirectSender;
import cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.fanout.FanoutSender;
import cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.topic.TopicSender;
import cn.cjd.springboot.modal.middleware.MQ.rabbit.persisitence.PersistenceSender;
import cn.cjd.springboot.modal.middleware.MQ.rabbit.priority.PrioritySender;
import cn.cjd.springboot.modal.middleware.MQ.rabbit.simple.queue.SimpleSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/rabbit")
public class RabbitTest {

    private final SimpleSender simpleSender;
    private final FanoutSender fanoutSender;
    private final DirectSender directSender;
    private final TopicSender topicSender;
    private final ConfirmSender confirmSender;
    private final PersistenceSender persistenceSender;
    private final PrioritySender prioritySender;
    private final DelaySender delaySender;

    @Autowired
    public RabbitTest(SimpleSender simpleSender, FanoutSender fanoutSender, DirectSender directSender, TopicSender topicSender, ConfirmSender confirmSender, PersistenceSender persistenceSender, PrioritySender prioritySender, DelaySender delaySender) {
        this.simpleSender = simpleSender;
        this.fanoutSender = fanoutSender;
        this.directSender = directSender;
        this.topicSender = topicSender;
        this.confirmSender = confirmSender;
        this.persistenceSender = persistenceSender;
        this.prioritySender = prioritySender;
        this.delaySender = delaySender;
    }

    /**
     * 简单队列
     */
    @GetMapping("/simple/send")
    public void simpleSend() {
        simpleSender.simpleSend();
        simpleSender.simpleSendWithChannel();
    }

    /**
     * 广播
     */
    @GetMapping("/fanout/send")
    public void fanoutSend() {
        fanoutSender.fanoutSend();
    }

    /**
     * 路由
     */
    @GetMapping("/direct/send")
    public void directSend() {
        directSender.directSend();
    }

    /**
     * 主题
     */
    @GetMapping("/topic/send")
    public void topicSend() {
        topicSender.topicSend();
    }

    /**
     * 消息发送确定
     * 消息接收ack
     */
    @GetMapping("/confirm/send")
    public void confirmSend() {
        confirmSender.confirmSend();
    }

    /**
     * 消息持久化
     */
    @GetMapping("/persistent/send")
    public void persistentSend() {
        persistenceSender.persistentSend();
    }

    /**
     * 消息优先级
     */
    @GetMapping("/priority/send")
    public void prioritySender() {
        prioritySender.prioritySend();
    }

    /**
     * 延时队列
     */
    @GetMapping("/delay/send")
    public void delayQueueSend() {
        delaySender.delaySend();
    }
}
