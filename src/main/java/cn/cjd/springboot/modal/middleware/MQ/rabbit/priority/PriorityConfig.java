package cn.cjd.springboot.modal.middleware.MQ.rabbit.priority;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Slf4j
@Configuration
public class PriorityConfig {

    public static final String PRIORITY_EXCHANGE = "priority_exchange";

    public static final String PRIORITY_QUEUE_NAME = "priority_queue";
    public static final String PRIORITY_QUEUE_KEY = "priority_queue_key";

    @Bean
    DirectExchange persistentExchange() {
        return new DirectExchange(PRIORITY_EXCHANGE, true, false);
    }

    @Bean
    Queue persistentQueue() {
        return new Queue(PRIORITY_QUEUE_NAME, true, false, false, new HashMap<String, Object>(){{put("x-max-priority", 10);}});
    }

    @Bean
    Binding persistentQueueBinding(Queue persistentQueue, DirectExchange persistentExchange) {
        return BindingBuilder.bind(persistentQueue).to(persistentExchange).with(PRIORITY_QUEUE_KEY);
    }
}
