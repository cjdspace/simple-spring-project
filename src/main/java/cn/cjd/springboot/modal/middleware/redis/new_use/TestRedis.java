package cn.cjd.springboot.modal.middleware.redis.new_use;

import cn.cjd.springboot.modal.common.utils.simpleUtils.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestRedis {

    @Autowired
    private RedisLock redisLock;

    private static final String  cycleFlowTaskExecuteID = UUIDUtil.getUUID().replaceAll("-","");

    public void main() {

        try {
            // 基于redis锁确保修改互斥,下面这行代码表示将这个id对应的payOrder锁起来，不允许这个id的数据同时进行操作
            redisLock.tryLock(null, cycleFlowTaskExecuteID);
//            if (CacheUtil.getCache().get("isUpdate") == null){
//                CacheUtil.getCache().set("isUpdate",1);
//                cycleTaskConfProvider.executeInternal(2);
//            }
//            if ("1".equals(isUpdate.toString())) {
//                isUpdate = 2;
//                //taskType 流程型任务生成
//                cycleTaskConfProvider.executeInternal(2);
//            }
        } catch (Exception e) {
//            logger.error("巡检任务异常！", e);
        } finally {
//            CacheUtil.getCache().del("isUpdate");
//            isUpdate = 1;
            redisLock.releaseLock(null, cycleFlowTaskExecuteID);
        }

    }
}
