package cn.cjd.springboot.modal.middleware.MQ.rabbit.confirm;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ConfirmConfig {

    public static final String CONFIRM_EXCHANGE = "confirm_exchange";

    public static final String CONFIRM_QUEUE_NAME = "confirm_queue";
    public static final String CONFIRM_QUEUE_KEY = "confirm_queue_key";

    @Bean
    DirectExchange ConfirmExchange() {
        return new DirectExchange(CONFIRM_EXCHANGE);
    }

    @Bean
    Queue confirmQueue() {
        return new Queue(CONFIRM_QUEUE_NAME);
    }

    @Bean
    Binding confirmQueueBinding(Queue confirmQueue, DirectExchange ConfirmExchange) {
        return BindingBuilder.bind(confirmQueue).to(ConfirmExchange).with(CONFIRM_QUEUE_KEY);
    }
}
