package cn.cjd.springboot.modal.middleware.MQ.rabbit.confirm;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@SuppressWarnings("unused")
@Slf4j
@Component
public class ConfirmSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public ConfirmSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * confirmCallback
     *
     * 消息只要被 rabbitmq broker 接收到就会触发confirmCallback回调。
     */
    private final RabbitTemplate.ConfirmCallback confirmCallback = (correlationData, ack, cause) -> {
        log.info("confirmCallback correlationData: {}, ack: {}, cause: {}", JSON.toJSONString(correlationData), ack, cause);
        if (!ack) {
            log.error("send message to broker failed, case: {}", cause);
            log.error("异常处理....");
        }
    };

    /**
     * returnCallback
     *
     * 消息从Exchange分发到Queue失败时才会有returnCallback，如果分发成功时不会调用returnCallback的
     *
     * 如果rabbitmq.template.mandatory: true, 消费者在消息没有被路由到合适队列情况下会被return监听，而不会自动删除
     */
    private final RabbitTemplate.ReturnCallback returnCallback = (message, replyCode, replyText, exchange, routingKey) -> {
        log.error("returnCallback exchange: {}, routingKey: {}, replyCode: {}, replyText: {}", exchange, routingKey, replyCode, replyText);
        log.error("Exchange<{}> send message to Queue failed with routeKey<{}>", exchange, routingKey);
    };

    public void confirmSend() {
        CorrelationData correlationData = new CorrelationData("0123456789"+new Date());
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        rabbitTemplate.setConfirmCallback(confirmCallback);
        rabbitTemplate.setReturnCallback(returnCallback);
        String message= "confirmSend";
        rabbitTemplate.convertAndSend(ConfirmConfig.CONFIRM_EXCHANGE, ConfirmConfig.CONFIRM_QUEUE_KEY, message, correlationData);
    }

}
