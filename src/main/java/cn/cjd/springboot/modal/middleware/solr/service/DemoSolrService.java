package cn.cjd.springboot.modal.middleware.solr.service;

import cn.cjd.springboot.modal.middleware.solr.document.DemoDocument;
import cn.cjd.springboot.modal.middleware.solr.query.SolrQueryParam;
import org.apache.solr.client.solrj.SolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DemoSolrService implements SolrService<DemoDocument>{

    private final SolrClient solrClient;

    @Autowired
    public DemoSolrService(@Qualifier(value = "mySolrClient")SolrClient solrClient) {
        this.solrClient = solrClient;
    }

    public void addDemoDocument() {
        DemoDocument demoDocument = new DemoDocument();
        demoDocument.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        demoDocument.setName("name1");
        addBean(demoDocument);
    }

    @Override
    public void addBean(DemoDocument entity) {
        try {
            solrClient.addBean(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addBeans(List<DemoDocument> demoDocuments) {
        try {
            solrClient.addBeans(demoDocuments);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public DemoDocument getById(String id) {
        return null;
    }

    @Override
    public void delById(String id) {

    }

    @Override
    public List<DemoDocument> queryFormSolr(SolrQueryParam solrQueryParam) {
        return null;
    }
}
