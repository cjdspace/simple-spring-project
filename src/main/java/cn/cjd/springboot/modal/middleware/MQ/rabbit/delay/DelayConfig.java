package cn.cjd.springboot.modal.middleware.MQ.rabbit.delay;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class DelayConfig {

    public static final String DEAD_EXCHANGE = "dead_exchange";
    public static final String DEAD_QUEUE_NAME = "dead_queue";
    public static final String DELAY_QUEUE_NAME = "delay_queue";
    public static final String DELAY_QUEUE_ROUTE_KEY = "delay_queue_key";

    @Bean
    DirectExchange deadExchange() {
        return new DirectExchange(DEAD_EXCHANGE);
    }

    @Bean
    Queue deadQueue() {
        return new Queue(DEAD_QUEUE_NAME, true, false, false, new HashMap<String, Object>(){{
            // 延时10000毫秒
            put("x-message-ttl", 10000);
            // 死信交换机
            put("x-dead-letter-exchange", "dead_exchange");
            // 死信交换机路由的key
            put("x-dead-letter-routing-key", DELAY_QUEUE_ROUTE_KEY);
        }});
    }

    @Bean
    Queue delayQueue() {
        return new Queue(DELAY_QUEUE_NAME);
    }

    @Bean
    Binding delayQueueBinding(Queue delayQueue, DirectExchange deadExchange) {
        return BindingBuilder.bind(delayQueue).to(deadExchange).with(DELAY_QUEUE_ROUTE_KEY);
    }
}
