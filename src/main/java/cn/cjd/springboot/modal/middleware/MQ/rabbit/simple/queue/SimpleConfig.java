package cn.cjd.springboot.modal.middleware.MQ.rabbit.simple.queue;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class SimpleConfig {

    public static final String SIMPLE_QUEUE1_NAME = "simple";
    public static final String SIMPLE_QUEUE1_NAME_CHANNEL = "simple_channel";

    @Bean
    public Queue SimpleQueue1() {
        log.info("创建简单队列：{}", SIMPLE_QUEUE1_NAME);
        return new Queue(SIMPLE_QUEUE1_NAME);
    }

    @Bean
    public Queue SimpleQueue1Channel() {
        log.info("创建简单队列：{}", SIMPLE_QUEUE1_NAME_CHANNEL);
        return new Queue(SIMPLE_QUEUE1_NAME_CHANNEL);
    }
}
