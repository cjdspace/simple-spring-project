package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.topic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TopicSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public TopicSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void topicSend() {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        String message1 = "speedy.write.rabbit";
        rabbitTemplate.convertAndSend(TopicConfig.TOPIC_EXCHANGE_NAME, message1, message1);
        String message2 = "lazy.grey.tortoise";
        rabbitTemplate.convertAndSend(TopicConfig.TOPIC_EXCHANGE_NAME, message2, message2);
        String message3 = "speedy.orange.bird";
        rabbitTemplate.convertAndSend(TopicConfig.TOPIC_EXCHANGE_NAME, message3, message3);
    }
}
