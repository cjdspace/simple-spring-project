package cn.cjd.springboot.modal.middleware.MQ.rabbit.persisitence;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PersistenceSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public PersistenceSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * 其实不需要手动设置持久化： message1.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
     *
     * rabbitTemplate 发送消息默认 delivery_mode: 2 是持久化的
     */
    public void persistentSend() {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        String message = "persistentSend";
        rabbitTemplate.convertAndSend(PersistenceConfig.PERSISTENT_EXCHANGE, PersistenceConfig.PERSISTENT_QUEUE_KEY, message, message1 -> {
            message1.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            return message1;
        });
    }
}
