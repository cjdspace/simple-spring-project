package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.fanout;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class FanoutConfig {

    public static final String FANOUT_EXCHANGE_NAME = "fanout_exchange";

    public static final String FANOUT_QUEUE1_NAME = "fanout_queue1";
    public static final String FANOUT_QUEUE2_NAME = "fanout_queue2";
    public static final String FANOUT_QUEUE3_NAME = "fanout_queue3";

    @Bean
    FanoutExchange FanoutExchange() {
        log.info("创建Fanout交换机：{}", FANOUT_EXCHANGE_NAME);
        return new FanoutExchange(FANOUT_EXCHANGE_NAME);
    }

    @Bean
    public Queue FanoutQueue1() {
        log.info("创建Fanout队列1：{}", FANOUT_QUEUE1_NAME);
        return new Queue(FANOUT_QUEUE1_NAME);
    }

    @Bean
    public Queue FanoutQueue2() {
        log.info("创建Fanout队列2：{}", FANOUT_QUEUE2_NAME);
        return new Queue(FANOUT_QUEUE2_NAME);
    }

    @Bean
    public Queue FanoutQueue3() {
        log.info("创建Fanout队列3：{}", FANOUT_QUEUE3_NAME);
        return new Queue(FANOUT_QUEUE3_NAME);
    }

    @Bean
    Binding bindingFanoutQueue1() {
        return BindingBuilder.bind(FanoutQueue1()).to(FanoutExchange());
    }

    @Bean
    Binding bindingFanoutQueue2() {
        return BindingBuilder.bind(FanoutQueue2()).to(FanoutExchange());
    }

    @Bean
    Binding bindingFanoutQueue3() {
        return BindingBuilder.bind(FanoutQueue3()).to(FanoutExchange());
    }
}

