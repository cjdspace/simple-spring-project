package cn.cjd.springboot.modal.middleware.MQ.rabbit.confirm;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class ConfirmReceive {

    @RabbitListener(queues = ConfirmConfig.CONFIRM_QUEUE_NAME)
    public void ConfirmQueueReceive(String msg, Channel channel, Message message) throws IOException {
        try {
            log.info("收到confirmQueue队列消息: {}", msg);
            Thread.sleep(5000L);
            int a = 1 / 0;
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            if (message.getMessageProperties().getRedelivered()) {
                log.error("消息已重复处理失败,拒绝再次接收...");
                channel.basicReject(message.getMessageProperties().getDeliveryTag(), false); // 拒绝消息
            } else {
                log.error("消息即将再次返回队列处理...");
                channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
            }

        }
    }

    /**
     *  消费消息有三种回执方法，我们来分析一下每种方法的含义。
     *  1、basicAck
     *      basicAck：表示成功确认，使用此回执方法后，消息会被rabbitmq broker 删除。
     *
     *      void basicAck(long deliveryTag, boolean multiple)
     *
     *      deliveryTag：表示消息投递序号，每次消费消息或者消息重新投递后，deliveryTag都会增加。手动消息确认模式下，我们可以对指定deliveryTag的消息进行ack、nack、reject等操作。
     *
     *      multiple：是否批量确认，值为 true 则会一次性 ack所有小于当前消息 deliveryTag 的消息。
     *
     *      举个栗子： 假设我先发送三条消息deliveryTag分别是5、6、7，可它们都没有被确认，当我发第四条消息此时deliveryTag为8，multiple设置为 true，会将5、6、7、8的消息全部进行确认。
     *
     *  2、basicNack
     *      basicNack ：表示失败确认，一般在消费消息业务异常时用到此方法，可以将消息重新投递入队列。
     *
     *      void basicNack(long deliveryTag, boolean multiple, boolean requeue)
     *
     *      deliveryTag：表示消息投递序号。multiple：是否批量确认。requeue：值为 true 消息将重新入队列。
     *
     *  3、basicReject
     *      basicReject：拒绝消息，与basicNack区别在于不能进行批量操作，其他用法很相似。
     *
     *      void basicReject(long deliveryTag, boolean requeue)
     *
     *      deliveryTag：表示消息投递序号。   requeue：值为 true 消息将重新入队列。
     **/
}
