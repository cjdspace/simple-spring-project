package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.direct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@SuppressWarnings("unused")
@Slf4j
@Component
public class DirectConfig {

    public static final String DIRECT_EXCHANGE_NAME = "direct_exchange";

    public static final String DIRECT_QUEUE1_NAME = "direct_queue1";
    public static final String DIRECT_QUEUE1_KEY = "direct_queue1_key";

    public static final String DIRECT_QUEUE2_NAME = "direct_queue2";
    public static final String DIRECT_QUEUE2_KEY = "direct_queue2_key";

    @Bean
    DirectExchange DirectExchange() {
        log.info("创建Direct交换机：{}", DIRECT_EXCHANGE_NAME);
        return new DirectExchange(DIRECT_EXCHANGE_NAME);
    }

    @Bean
    Queue directQueue1() {
        log.info("创建Direct队列1：{}", DIRECT_QUEUE1_NAME);
        return new Queue(DIRECT_QUEUE1_NAME);
    }

    @Bean
    Queue DirectQueue2() {
        log.info("创建Direct队列1：{}", DIRECT_QUEUE2_NAME);
        return new Queue(DIRECT_QUEUE2_NAME);
    }

    @Bean
    Binding directQueue1BindingExchange(@Qualifier("directQueue1") Queue directQueue1) {
        log.info("将队列: {} 绑定到交换机: {}, routeKey: {}", DIRECT_QUEUE1_NAME, DIRECT_EXCHANGE_NAME, DIRECT_QUEUE1_KEY);
        return BindingBuilder.bind(directQueue1).to(DirectExchange()).with(DIRECT_QUEUE1_KEY);
    }

    @Bean
    Binding directQueue2BindingExchange() {
        log.info("将队列: {} 绑定到交换机: {}, routeKey: {}", DIRECT_QUEUE2_NAME, DIRECT_EXCHANGE_NAME, DIRECT_QUEUE2_KEY);
        return BindingBuilder.bind(DirectQueue2()).to(DirectExchange()).with(DIRECT_QUEUE2_KEY);
    }
}
