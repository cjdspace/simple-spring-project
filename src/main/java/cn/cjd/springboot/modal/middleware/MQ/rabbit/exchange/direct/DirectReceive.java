package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.direct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@SuppressWarnings("unused")
@Slf4j
@Component
public class DirectReceive {

    @RabbitHandler
    @RabbitListener(queues = DirectConfig.DIRECT_QUEUE1_NAME)
    public void DirectQueue1Receive(byte[] message) {
        String msg = new String(message, StandardCharsets.UTF_8);
        log.info("DirectQueue1Receive content: {}", msg);
    }

    @RabbitHandler
    @RabbitListener(queues = DirectConfig.DIRECT_QUEUE2_NAME)
    public void DirectQueue2Receive(byte[] message) {
        String msg = new String(message, StandardCharsets.UTF_8);
        log.info("DirectQueue2Receive content: {}", msg);
    }

}
