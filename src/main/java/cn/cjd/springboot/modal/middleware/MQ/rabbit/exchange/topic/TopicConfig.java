package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.topic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TopicConfig {

    public static final String TOPIC_EXCHANGE_NAME = "topic_exchange";

    public static final String TOPIC_QUEUE1_NAME = "topic_queue1";
    public static final String TOPIC_QUEUE1_KEY = "*.orange.*";

    public static final String TOPIC_QUEUE2_NAME = "topic_queue2";
    public static final String TOPIC_QUEUE2_KEY1 = "lazy.#";
    public static final String TOPIC_QUEUE2_KEY2 = "*.*.rabbit";

    @Bean
    TopicExchange topicExchange() {
        log.info("创建Topic交换机：{}", TOPIC_EXCHANGE_NAME);
        return new TopicExchange(TOPIC_EXCHANGE_NAME);
    }

    @Bean
    Queue topicQueue1() {
        log.info("创建Topic队列1：{}", TOPIC_QUEUE1_NAME);
        return new Queue(TOPIC_QUEUE1_NAME);
    }

    @Bean
    Queue topicQueue2() {
        log.info("创建Topic队列1：{}", TOPIC_QUEUE2_NAME);
        return new Queue(TOPIC_QUEUE2_NAME);
    }

    @Bean
    Binding topicQueue1ToExchange(TopicExchange topicExchange, Queue topicQueue1) {
        return BindingBuilder.bind(topicQueue1).to(topicExchange).with(TOPIC_QUEUE1_KEY);
    }

    @Bean
    Binding TopicQueue2ToExchangeKey1(TopicExchange topicExchange, Queue topicQueue2) {
        return BindingBuilder.bind(topicQueue2).to(topicExchange).with(TOPIC_QUEUE2_KEY1);
    }

    @Bean
    Binding TopicQueue2ToExchangeKey2(TopicExchange topicExchange, Queue topicQueue2) {
        return BindingBuilder.bind(topicQueue2).to(topicExchange).with(TOPIC_QUEUE2_KEY2);
    }
}
