package cn.cjd.springboot.modal.middleware.swagger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "/swagger", description="swagger接口")
@RestController
@RequestMapping("/swagger")
public class SwaggerInterface {

    @ApiOperation(value = "/print",notes = "打印hello world")
    @GetMapping(value = "/print")
    public Object testQueryDate(){
        System.out.println("hello world");
        return "hello world";
    }
}
