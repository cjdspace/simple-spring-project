package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.fanout;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FanoutSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public FanoutSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void fanoutSend() {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        for (int i = 0; i < 1; i++) {
            String message = "FanoutSender_" + i;
            rabbitTemplate.convertAndSend(FanoutConfig.FANOUT_EXCHANGE_NAME,"",message);
        }
    }
}
