package cn.cjd.springboot.modal.middleware.MQ.rabbit.persisitence;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Slf4j
//@Component
public class persistenceReceive {

//    @RabbitHandler
//    @RabbitListener(queues = PersistenceConfig.PERSISTENT_QUEUE_NAME)
    public void persistenceQueueReceive(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("persistenceQueueReceive content: {}", msg);
    }
}
