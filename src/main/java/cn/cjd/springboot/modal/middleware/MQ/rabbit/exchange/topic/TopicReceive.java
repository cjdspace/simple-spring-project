package cn.cjd.springboot.modal.middleware.MQ.rabbit.exchange.topic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Slf4j
@Component
public class TopicReceive {

    @RabbitHandler
    @RabbitListener(queues = TopicConfig.TOPIC_QUEUE1_NAME)
    public void TopicQueue1Receive(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("TopicQueue1Receive content: {}", msg);
    }

    @RabbitHandler
    @RabbitListener(queues = TopicConfig.TOPIC_QUEUE2_NAME)
    public void TopicQueue2Receive(byte[] bytes) {
        String msg = new String(bytes, StandardCharsets.UTF_8);
        log.info("TopicQueue2Receive content: {}", msg);
    }
}
