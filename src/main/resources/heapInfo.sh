#!/bin/bash
if [ $# -le 0 ]; then
    echo "usage: $0 <pid> [line-number]"
    exit 1
fi

# java home
if test -z $JAVA_HOME 
then
    JAVA_HOME='/usr/bin'
fi

# pid
pid=$1
echo "input pid is $pid, then use docker java pid"

# docker java pid
pid=$(top -bn1 | grep java | awk '{print $1}')
echo "docker java pid is $pid"

# file position
heapInfo=$PWD/heapInfo$pid.log
echo "heapInfo position is $heapInfo"

echo "------------------------------ heapInfo ------------------------------" >> $heapInfo
echo "\n" >> $heapInfo

echo "------------------------------ 类加载统计(-class) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -class $pid >> $heapInfo

echo "------------------------------ 编译统计(-compiler) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -compiler $pid >> $heapInfo

echo "------------------------------ JVM编译方法统计(-printcompilation) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -printcompilation $pid >> $heapInfo

echo "------------------------------ 堆内存统计(-gccapacity) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -gccapacity $pid >> $heapInfo

echo "------------------------------ 垃圾回收统计(-gc) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -gc $pid >> $heapInfo

echo "------------------------------ 新生代垃圾回收统计(-gcnew) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -gcnew $pid >> $heapInfo

echo "------------------------------ 新生代内存统计(-gcnewcapacity) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -gcnewcapacity $pid >> $heapInfo

echo "------------------------------ 老年代垃圾回收统计(-gcold) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -gcold $pid >> $heapInfo

echo "------------------------------ 老年代内存统计(-gcoldcapacity) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -gcoldcapacity $pid >> $heapInfo

echo "------------------------------ 元数据空间统计(-gcmetacapacity) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -gcmetacapacity $pid >> $heapInfo

echo "------------------------------ 总结垃圾回收统计(-gcutil) ------------------------------" >> $heapInfo
$JAVA_HOME/bin/jstat -gcutil $pid >> $heapInfo







