#!/bin/bash

# docker java pid
pid=$(top -bn1 | grep java | awk '{print $1}')
echo "docker java pid is $pid"

# file position
dumpfile=$PWD/dump$pid
echo "dumpfile position is $dumpfile"

echo "start to Heap dump"

jmap -dump:format=b,file=@dumpfile $pid
